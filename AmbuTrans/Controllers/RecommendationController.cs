﻿using AmbuTrans.DataLayer.Repository;
using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AmbuTrans.Controllers
{
    public class RecommendationController : Controller
    {
        private readonly RecommendationRepository _repo;
        public RecommendationController()
        {
            _repo = new RecommendationRepository();
        }
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                ViewBag.breadcrumbModel = new BreadcrumbModel() { subActionTitle = "View Recommendation" };
                IQueryable<RecomdationCategoryModel> List = _repo.GetRecomdationList();
                return View(List);
            }
            else
                return RedirectToAction("Login", "AdminAccount");

        }
        [HttpGet]
        public ActionResult Add()
        {
            if (Session["UserID"] != null)
            {
                ViewBag.Action = "Add";
                ViewBag.breadcrumbModel = new BreadcrumbModel()
                {
                    actioncontroller = "Recommendation",
                    actionName = "Index",
                    actionTitle = "Recommendation",
                    subActionTitle = "Add"
                };
                return View("AddorUpdate");
            }
            return RedirectToAction("Login", "AdminAccount");

        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            AddUpdateRecomdationModel model = _repo.FindById(Id);

            if (model != null)
            {
                ViewBag.Action = "Edit";
                ViewBag.breadcrumbModel = new BreadcrumbModel()
                {
                    actioncontroller = "Recommendation",
                    actionName = "Index",
                    actionTitle = "Recommendation",
                    subActionTitle = "Edit"
                };
                return View("AddorUpdate", model);
            }
            return Redirect(Url.Action("Index"));
        }
        [HttpPost]
        public ActionResult Edit(AddUpdateRecomdationModel model)
        {
            if (ModelState.IsValid)
            {
               
                ResponseModel<AddUpdateRecomdationModel> mResult = new ResponseModel<AddUpdateRecomdationModel>();
                bool obj = Convert.ToBoolean(_repo.AddorUpdate(model));
                if (obj == true)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Record Updated successfully";
                }
                TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                {
                    return Redirect(Url.Action("Index"));
                }
            }
            ViewBag.Action = "Edit";
            ViewBag.breadcrumbModel = new BreadcrumbModel()
            {
                actioncontroller = "Recommendation",
                actionName = "Index",
                actionTitle = "Recommendation",
                subActionTitle = "Edit"
            };

            return View("AddorUpdate", model);
        }

        [HttpPost]
        public ActionResult Add(AddUpdateRecomdationModel model, HttpPostedFileBase FilUpload)
        {
            if (ModelState.IsValid)
            {
                ResponseModel<AddUpdateRecomdationModel> mResult = new ResponseModel<AddUpdateRecomdationModel>();

                bool obj = Convert.ToBoolean(_repo.AddorUpdate(model));
                if (obj == true)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Record Add successfully";
                }
                TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            ViewBag.Action = "Add";
            ViewBag.breadcrumbModel = new BreadcrumbModel()
            {
                actioncontroller = "Recommendation",
                actionName = "Index",
                actionTitle = "Recommendation",
                subActionTitle = "Add"
            };

            return View("AddorUpdate", model);
        }

        public JsonResult Delete(int Id)
        {
            ResponseModel<object> mResult = _repo.Delete(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }
    }
}