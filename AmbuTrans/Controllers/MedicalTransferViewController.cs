﻿using AmbuTrans.DataLayer.Repository;
using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AmbuTrans.Controllers
{
    public class MedicalTransferViewController : Controller
    {
        private readonly MedicalTransferRepository _repo;
        public MedicalTransferViewController()
        {

            _repo = new MedicalTransferRepository();
        }
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                ViewBag.breadcrumbModel = new BreadcrumbModel() { subActionTitle = "View Patient Details" };
                IQueryable<ViewMedicalTransferFormModel> List = _repo.GetAllMedicalTransferDetails();
                return View(List);
            }
            else
                return RedirectToAction("Login", "AdminAccount");
        }
        public ActionResult ReportView(int MedicalTransferID)
        {
            ResponseModel<ViewMedicalTransferModel> mResult = _repo.ViewMedicalTransferReport(MedicalTransferID);
            return View(mResult.Result);
        }
        public ActionResult Open(int MedicalTransferID)
        {
            ResponseModel<bool> mResult = new ResponseModel<bool>();
            bool obj = _repo.OpenReport(MedicalTransferID);
            if (obj == true)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "User open.";
            }
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            return Redirect(Url.Action("Index"));

        }
        public ActionResult Closed(int MedicalTransferID)
        {
            ResponseModel<bool> mResult = new ResponseModel<bool>();
            bool obj = _repo.ClosedReport(MedicalTransferID);
            if (obj == true)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "User Closed.";
            }
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            return Redirect(Url.Action("Index"));
        }
        [HttpPost]
        public JsonResult Delete(int Id)
        {
            ResponseModel<object> mResult = _repo.DeleteTransferDetails(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

    }
}