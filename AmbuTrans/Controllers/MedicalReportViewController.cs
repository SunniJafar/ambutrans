﻿using AmbuTrans.DataLayer.Repository;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AmbuTrans.Controllers
{
    public class MedicalReportViewController : Controller
    {
        private readonly MedicalReportRepository _repo;
        public MedicalReportViewController()
        {
            _repo = new MedicalReportRepository();
        }
        public ActionResult ReportView(int MedicalTransferID)
        {
            ResponseModel<List<ViewMedicalReport>> mResult = _repo.ViewMedicalReport(MedicalTransferID);
            return View(mResult.Result);
        }
    }
}