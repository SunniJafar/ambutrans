﻿using AmbuTrans.DataLayer.Repository;
using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AmbuTrans.Controllers
{
    public class DoctorController : Controller
    {
        private readonly DoctorRepository _repo;
        public DoctorController()
        {
            _repo = new DoctorRepository();
        }
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                ViewBag.breadcrumbModel = new BreadcrumbModel() { subActionTitle = "View Employee" };
                IQueryable<ViewDoctorModel> List = _repo.GetDoctorList();
                return View(List);
            }
            else
                return RedirectToAction("Login", "AdminAccount");
        }
        [HttpGet]
        public ActionResult Add()
        {
            if (Session["UserID"] != null)
            {
                ViewBag.Action = "Add";
                ViewBag.breadcrumbModel = new BreadcrumbModel()
                {
                    actioncontroller = "Doctor",
                    actionName = "Index",
                    actionTitle = "Employee",
                    subActionTitle = "Add"
                };
                return View("AddorUpdate");
            }
            else
                return RedirectToAction("Login", "AdminAccount");

        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            AddUpdateDoctorModel model = _repo.FindById(Id);

            if (model != null)
            {
                ViewBag.Action = "Edit";
                ViewBag.breadcrumbModel = new BreadcrumbModel()
                {
                    actioncontroller = "Doctor",
                    actionName = "Index",
                    actionTitle = "Employee",
                    subActionTitle = "Edit"
                };
                return View("AddorUpdate", model);
            }
            return Redirect(Url.Action("Index"));
        }
        [HttpPost]
        public ActionResult Edit(AddUpdateDoctorModel model)
        {
            if (ModelState.IsValid)
            {

                ResponseModel<AddUpdateDoctorModel> mResult = new ResponseModel<AddUpdateDoctorModel>();
                bool obj = Convert.ToBoolean(_repo.AddorUpdate(model));
                if (obj == true)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Record Updated successfully.";
                }
                TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                {
                    return Redirect(Url.Action("Index"));
                }
            }
            ViewBag.Action = "Edit";
            ViewBag.breadcrumbModel = new BreadcrumbModel()
            {
                actioncontroller = "Doctor",
                actionName = "Index",
                actionTitle = "Employee",
                subActionTitle = "Edit"
            };

            return View("AddorUpdate", model);
        }

        [HttpPost]
        public ActionResult Add(AddUpdateDoctorModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseModel<object> mResult = new ResponseModel<object>();
                bool obj = Convert.ToBoolean(_repo.AddorUpdate(model));
                if (obj == true)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Record add successfully.";
                }
                TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            ViewBag.Action = "Add";
            ViewBag.breadcrumbModel = new BreadcrumbModel()
            {
                actioncontroller = "Doctor",
                actionName = "Index",
                actionTitle = "Employee",
                subActionTitle = "Add"
            };

            return View("AddorUpdate", model);
        }
        [HttpPost]
        public JsonResult Delete(int Id)
        {
            ResponseModel<object> mResult = _repo.Delete(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Activate(int DoctorID)
        {
            ResponseModel<bool> mResult = new ResponseModel<bool>();
            bool obj = _repo.Activate(DoctorID);
            if (obj == true)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Employee Activated";
            }
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            return Redirect(Url.Action("Index"));

        }
        public ActionResult Deactivate(int DoctorID)
        {
            ResponseModel<bool> mResult = new ResponseModel<bool>();
            bool obj = _repo.Deactivate(DoctorID);
            if (obj == true)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Doctor Deactivated";
            }
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            return Redirect(Url.Action("Index"));
        }
    }
}