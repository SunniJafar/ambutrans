﻿using AmbuTrans.DataLayer.Repository;
using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AmbuTrans.Controllers.API
{
    public class RecommendationSubcategoryController : Controller
    {
        private readonly RecommendationSubcategoryRepository _repo;
        RecommendationRepository _Recomdation = new RecommendationRepository();
        public RecommendationSubcategoryController()
        {
            _repo = new RecommendationSubcategoryRepository();
        }
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                ViewBag.breadcrumbModel = new BreadcrumbModel() { subActionTitle = "View Recommendation Subcategory" };
                IQueryable<ViewRecomdationSubcategoryModel> category = _repo.GetSubcategoryList();
                return View(category);
            }
            return RedirectToAction("Login", "AdminAccount");
        }
        [HttpGet]
        public ActionResult Edit(int Id, string filter)
        {

            ViewData["Recommendation"] = _Recomdation.GetRecomdationList().OrderBy(x => x.RecomdationName).ToList();
            AddUpdateRecomdationSubcategoryModel model = _repo.FindById(Id);

            if (model != null)
            {
                ViewBag.Action = "Edit";
                ViewBag.breadcrumbModel = new BreadcrumbModel()
                {
                    actioncontroller = "RecommendationSubcategory",
                    actionName = "Index",
                    actionTitle = "RecommendationSubcategory",
                    subActionTitle = "Edit"
                };


                return View("AddorUpdate", model);
            }
            return Redirect(Url.Action("Index") + "?" + filter);
        }
        [HttpPost]
        public ActionResult Edit(AddUpdateRecomdationSubcategoryModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseModel<AddUpdateRecomdationSubcategoryModel> mResult = new ResponseModel<AddUpdateRecomdationSubcategoryModel>();
                bool obj = Convert.ToBoolean(_repo.AddorUpdate(model));
                if (obj == true)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Subcategory Updated successfully";
                }
                TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                {
                    return Redirect(Url.Action("Index"));
                }
            }
            ViewData["Recommendation"] = _Recomdation.GetRecomdationList().OrderBy(x => x.RecomdationName).ToList();
            ViewBag.Action = "Edit";
            ViewBag.breadcrumbModel = new BreadcrumbModel()
            {
                actioncontroller = "RecommendationSubcategory",
                actionName = "Index",
                actionTitle = "RecommendationSubcategory",
                subActionTitle = "Edit"
            };

            return View("AddorUpdate", model);
        }
        [HttpPost]
        public ActionResult Add(AddUpdateRecomdationSubcategoryModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseModel<AddUpdateRecomdationSubcategoryModel> mResult = new ResponseModel<AddUpdateRecomdationSubcategoryModel>();

                bool obj = Convert.ToBoolean(_repo.AddorUpdate(model));
                if (obj == true)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Subcategory Add successfully";
                }
                TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            ViewData["Recommendation"] = _Recomdation.GetRecomdationList().OrderBy(x => x.RecomdationName).ToList();
            ViewBag.Action = "Add";
            ViewBag.breadcrumbModel = new BreadcrumbModel()
            {
                actioncontroller = "RecommendationSubcategory",
                actionName = "Index",
                actionTitle = "RecommendationSubcategory",
                subActionTitle = "Add"
            };

            return View("AddorUpdate", model);

        }
        public ActionResult Add()
        {
            if (Session["UserID"] != null)
            {
                ViewData["Recommendation"] = _Recomdation.GetRecomdationList().OrderBy(x => x.RecomdationName).ToList();

                ViewBag.Action = "Add";
                ViewBag.breadcrumbModel = new BreadcrumbModel()
                {
                    actioncontroller = "RecommendationSubcategory",
                    actionName = "Index",
                    actionTitle = "RecommendationSubcategory",
                    subActionTitle = "Add"
                };
                return View("AddorUpdate");
            }
            return RedirectToAction("Login", "AdminAccount");
        }
        public JsonResult Delete(int Id)
        {
            ResponseModel<object> mResult = _repo.Delete(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }
    }
}