﻿using AmbuTrans.DataLayer.Repository;
using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AmbuTrans.Controllers
{
    public class AdminAccountController : Controller
    {
        private readonly AdminAccountRepository _repo;
        public AdminAccountController()
        {
            _repo = new AdminAccountRepository();
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login(string ReturnUrl)
        {
            ViewBag.ReturnUrl = ReturnUrl;
            if (!string.IsNullOrEmpty(ReturnUrl))
            {
                TempData["alertModel"] = new AlertModel(AlertStatus.warning, "You are unauthorised, Please login");
            }
            return View("Login");
        }
        [HttpPost]
        public async Task<ActionResult> Login(AdminLoginModel model, string ReturnUrl)
        {
            ResponseModel<AdminLoginModel> mResult = new ResponseModel<AdminLoginModel>();
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            if (_repo.AdminLogin(model))
            {
                Session["UserID"] = model.username.ToString();
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "User Admin Logged in successfully!";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Wrong Email or Password";
            }
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.info : AlertStatus.danger, mResult.Message);

            if (mResult.Status == ResponseStatus.Success)
            {
                if (!string.IsNullOrWhiteSpace(ReturnUrl) && Url.IsLocalUrl(ReturnUrl))
                    return RedirectPermanent(ReturnUrl);
                return RedirectToActionPermanent("Index", "Home");
            }
            return View();
        }

        public ActionResult ChangePassword()
        {
            ViewBag.subBreadcrumb_title = "Change Password";
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> ChangePassword(AdminChangePasswordModel model)
        {
            ResponseModel<AdminChangePasswordModel> mResult = new ResponseModel<AdminChangePasswordModel>();
            model.username = Session["UserID"].ToString();
            if (_repo.ChangePassword(model))
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Password changed successfully.";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Change password failed.";
            }
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);

            if (mResult.Status == ResponseStatus.Success)
            {
                return RedirectToActionPermanent("Index", "Home");
            }
            else
            {
                return View(model);
            }
        }
        [HttpGet]
        public ActionResult LogOut()
        {
            Session["UserID"] = null;
            return RedirectToActionPermanent("Login", "AdminAccount");
        }
    }
}