﻿using AmbuTrans.DataLayer.Repository;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AmbuTrans.Controllers
{
    public class VitalsInformationViewController : Controller
    {
        private readonly VitalsInformationRepository _repo;
        public VitalsInformationViewController()
        {
            _repo = new VitalsInformationRepository();
        }
        public ActionResult VitalsInformationView(int MedicalTransferID)
        {
            ResponseModel<EditVitalsInformationModel> mResult = _repo.FindById(MedicalTransferID);
            return View(mResult.Result);
        }
    }
}