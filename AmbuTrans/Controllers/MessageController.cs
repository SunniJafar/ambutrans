﻿using AmbuTrans.DataLayer.Repository;
using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AmbuTrans.Controllers
{
    public class MessageController : Controller
    {
        private readonly NotificationRepository _repo;
        public MessageController()
        {
            _repo = new NotificationRepository();
        }
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                ViewBag.breadcrumbModel = new BreadcrumbModel() { subActionTitle = "View Messages" };
                IQueryable<ViewMessageModel> List = _repo.GetMessageList();
                return View(List);
            }
            else
            {
                return RedirectToAction("Login", "AdminAccount");
            }
        }
        public ActionResult Message()
        {
            ViewBag.Action = "SendMessage";
            ViewBag.breadcrumbModel = new BreadcrumbModel()
            {
                actioncontroller = "Messnnage",
                actionName = "Index",
                actionTitle = "Message",
                subActionTitle = "Send Message"
            };
            return View();
        }
        [HttpPost]
        public ActionResult SendMessage(SendMessageModel model)
        {

            ResponseModel<SendMessageModel> mResult = new ResponseModel<SendMessageModel>();
            if (ModelState.IsValid)
            {
                bool obj = _repo.SendMessage(model);
                if (obj == true)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Send Message successfully";
                }
                TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                {
                    return Redirect(Url.Action("Index"));
                }

            }
            return Redirect(Url.Action("Index"));
        }
        public JsonResult Delete(int Id)
        {
            ResponseModel<object> mResult = _repo.Delete(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }
    }
}