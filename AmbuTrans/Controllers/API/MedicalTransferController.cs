﻿using AmbuTrans.DataLayer.Repository;
using AmbuTrans.Helper;
using AmbuTrans.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace AmbuTrans.Controllers
{
    [RoutePrefix("API/MedicalTransfer")]
    public class MedicalTransferController : ApiController
    {
        private readonly MedicalTransferRepository _repo;
        public MedicalTransferController()
        {
            _repo = new MedicalTransferRepository();
        }
        [HttpPost]
        [Route("AddUpdate")]
        public async Task<ResponseModel<object>> AddMedicalTransfer()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            //try
            {
                StringBuilder modelString = new StringBuilder();
                Dictionary<string, object> dicFormData = new Dictionary<string, object>();

                var provider = await Request.Content.ReadAsMultipartAsync<InMemoryMultipartFormDataStreamProvider>(new InMemoryMultipartFormDataStreamProvider());

                //access form data
                NameValueCollection formData = provider.FormData;

                //access files
                IList<HttpContent> files = provider.Files;

                // key-value pairs.
                foreach (var key in formData.AllKeys)
                {
                    if (!dicFormData.Keys.Contains(key))
                        dicFormData.Add(key, string.Empty);
                    foreach (var val in formData.GetValues(key))
                    {
                        if (key == "RecomdationSubcategoryTransaction")
                        {
                            dicFormData[key] = JsonConvert.DeserializeObject<List<RecomdationSubTransactionModel>>(val);
                        }
                        else if (key == "RepatriationNecessaryBecauseOf")
                        {
                            dicFormData[key] = JsonConvert.DeserializeObject<List<RepatriationNecessaryTransactionModel>>(val);
                        }
                        else
                        {
                            dicFormData[key] = val;
                        }
                        modelString.Append(string.Format("{0}: {1}", key, val));
                    }
                }
                string Json = JsonConvert.SerializeObject(dicFormData);
                AddMedicalTransferModel model = new AddMedicalTransferModel();
                try
                {
                    model = JsonConvert.DeserializeObject<AddMedicalTransferModel>(Json);
                }
                catch(Exception ex)
                {

                }

                #region save uploaded images

                string filename = String.Empty;
                foreach (HttpContent file in files)
                {
                    //to append any text in filename.
                    string ogFileName = file.Headers.ContentDisposition.FileName.Trim('\"');
                    string shortGuid = ShortGuid.NewGuid().ToString();
                    var thisFileName = DateTime.Now.ToString("yyyyMMddHHmmssfff_") + shortGuid + Path.GetExtension(ogFileName);

                    Stream input = await file.ReadAsStreamAsync();
                    byte[] filearray = await file.ReadAsByteArrayAsync();
                    filename = HttpContext.Current.Server.MapPath("~/Files/SignatureImages/") + thisFileName;

                    using (Stream fileStream = File.OpenWrite(filename))
                    {
                        input.CopyTo(fileStream);
                        //close file
                        fileStream.Close();
                    }

                    model.SignatureImageName = thisFileName;
                }

                #endregion save uploaded images

                mResult = _repo.AddUpdateMedicalTransfer(model);
            }
            //catch (Exception err)
            //{
            //    mResult.Message = err.Message;
            //    mResult.Status = ResponseStatus.Failed;
            //}
            return mResult;
        }
        [HttpPost]
        [Route("Update")]
        public async Task<ResponseModel<object>> UpdateMedicalTransfer()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            try
            {
                StringBuilder modelString = new StringBuilder();
                Dictionary<string, object> dicFormData = new Dictionary<string, object>();

                var provider = await Request.Content.ReadAsMultipartAsync<InMemoryMultipartFormDataStreamProvider>(new InMemoryMultipartFormDataStreamProvider());

                //access form data
                NameValueCollection formData = provider.FormData;

                //access files
                IList<HttpContent> files = provider.Files;

                // key-value pairs.
                foreach (var key in formData.AllKeys)
                {
                    if (!dicFormData.Keys.Contains(key))
                        dicFormData.Add(key, string.Empty);
                    foreach (var val in formData.GetValues(key))
                    {
                        if (key == "RecomdationSubcategoryTransaction")
                        {
                            dicFormData[key] = JsonConvert.DeserializeObject<List<RecomdationSubTransactionModel>>(val);
                        }
                        else if (key == "RepatriationNecessaryBecauseOf")
                        {
                            dicFormData[key] = JsonConvert.DeserializeObject<List<RepatriationNecessaryTransactionModel>>(val);
                        }
                        else
                        {
                            dicFormData[key] = val;
                        }
                        modelString.Append(string.Format("{0}: {1}", key, val));
                    }
                }
                string Json = Newtonsoft.Json.JsonConvert.SerializeObject(dicFormData);
                EditMedicalTransferModel model = JsonConvert.DeserializeObject<EditMedicalTransferModel>(Json);


                string filename = String.Empty;
                foreach (HttpContent file in files)
                {
                    //to append any text in filename.
                    string ogFileName = file.Headers.ContentDisposition.FileName.Trim('\"');
                    string shortGuid = ShortGuid.NewGuid().ToString();
                    var thisFileName = DateTime.Now.ToString("yyyyMMddHHmmssfff_") + shortGuid + Path.GetExtension(ogFileName);

                    Stream input = await file.ReadAsStreamAsync();
                    byte[] filearray = await file.ReadAsByteArrayAsync();
                    filename = Path.Combine(HttpContext.Current.Server.MapPath("~/Files/SignatureImages/"), thisFileName);
                    using (Stream fileStream = File.OpenWrite(filename))
                    {
                        input.CopyTo(fileStream);
                        //close file
                        fileStream.Close();
                    }

                    model.SignatureImageName = thisFileName;

                }
                mResult = _repo.UpdateDetails(model);
            }
            catch (Exception err)
            {
                mResult.Message = err.Message;
                mResult.Status = ResponseStatus.Failed;
            }
            return mResult;
        }

        [HttpGet]
        [Route("Delete")]
        public ResponseModel<object> DeleteMedicalTransfer(int MedicalTransferID)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            bool result = _repo.DeleteRecord(MedicalTransferID);
            if (result)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Record deleted.";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Record not deleted.";
            }
            return mResult;

        }
        [HttpGet]
        [Route("SelectedMedicalTransfer")]
        public ResponseModel<ViewMedicalTransferModel> SelectedMedicalTransfer(int MedicalTransferID)
        {
            ResponseModel<ViewMedicalTransferModel> mResult = new ResponseModel<ViewMedicalTransferModel>();
            mResult = _repo.FindById(MedicalTransferID);
            return mResult;
        }

        [HttpGet]
        [Route("FinalSubmitForm")]
        public ResponseModel<object> FinalSubmitForm(int MedicalTransferID)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            string MedicalTransferURL = "http://ambutrans.imperoserver.in/MedicalTransferView/ReportView?MedicalTransferID=" + MedicalTransferID;
            string VitalsInformationURL = "http://ambutrans.imperoserver.in/VitalsInformationView/VitalsInformationView?MedicalTransferID=" + MedicalTransferID;
            string MedicalReportView = "http://ambutrans.imperoserver.in/MedicalReportView/ReportView?MedicalTransferID=" + MedicalTransferID;
            string outputfolder = HttpContext.Current.Server.MapPath("~/Files/PDF/");
            var PDFFilePath = PdfGenerator.HtmlToPdf(pdfOutputLocation: outputfolder,
            outputFilenamePrefix: "PDF_" + MedicalTransferID,
            urls: new string[] { MedicalTransferURL, VitalsInformationURL, MedicalReportView });
            if (PDFFilePath != string.Empty)
            {
                bool result = _repo.AddPdfIndatabase(MedicalTransferID, PDFFilePath);
                if (result)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "PDF  Generated";
                }
                else
                    mResult.Status = ResponseStatus.Failed;
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "PDF Not Generated";
            }
            return mResult;
        }

        [HttpGet]
        [Route("GetPdf")]
        public ResponseModel<ViewPdfModel> GetPdf(int MedicalTransferID)
        {
            ResponseModel<ViewPdfModel> mResult = new ResponseModel<ViewPdfModel>();
            mResult = _repo.GetPdf(MedicalTransferID);
            return mResult;
        }
    }
}
