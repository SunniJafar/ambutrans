﻿using AmbuTrans.DataLayer.Repository;
using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AmbuTrans.Controllers
{
    [RoutePrefix("API/Account")]
    public class AccountController : ApiController
    {
        private readonly AccountRepository _repo;
        public AccountController()
        {
            _repo = new AccountRepository();
        }

        [HttpPost]
        [Route("Register")]
        public ResponseModel<object> Registeration(RegisterModel model)
        {
            return _repo.AddUser(model);
        }

        [HttpPost]
        [Route("Login")]
        public ResponseModel<LoginModel> Login(LoginModel model)
        {
            return  _repo.UserLogin(model);
        }
        [Route("ChangePassword")]
        public ResponseModel<ChangePasswordModel> ChangePassword(ChangePasswordModel model)
        {
            ResponseModel<ChangePasswordModel> mResult = new ResponseModel<ChangePasswordModel>();
            if (_repo.ChangePassword(model))
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Password Changed successfully!";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Password Not Changed";
            }
            return mResult;
        }
        [HttpPost]
        [Route("ForgetPassword")]
        public ResponseModel<ForgetPasswordModel> ForgetPassword(ForgetPasswordModel model)
        {
            ResponseModel<ForgetPasswordModel> mResult = new ResponseModel<ForgetPasswordModel>();
            if (_repo.ForgetUserPassword(model))
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Password Send successfully!";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Please enter correct EmailId!";
            }
            return mResult;
        }

        [HttpGet]
        [Route("Logout")]
        public ResponseModel<object> Logout(int DoctorID)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (_repo.Logout(DoctorID))
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Logout successfully!";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
            }
            return mResult;
        }
    }
}
