﻿using AmbuTrans.DataLayer.Repository;
using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AmbuTrans.Controllers.API
{
    [RoutePrefix("API")]
    public class NotificationController : ApiController
    {
        private readonly NotificationRepository _repo;
        public NotificationController()
        {
            _repo = new NotificationRepository();
        }
        [HttpGet]
        [Route("ViewNotification")]
        public ResponseModel<List<ViewNotificationModel>> ViewNotification(int DoctorID)
        {
            ResponseModel<List<ViewNotificationModel>> mResult = new ResponseModel<List<ViewNotificationModel>>();
            mResult = _repo.GetMessage(DoctorID);
            if (mResult.Result.Count > 0)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = mResult.Result.Count + " Record listed.";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "No record listed.";
            }
            return mResult;
        }
    }
}
