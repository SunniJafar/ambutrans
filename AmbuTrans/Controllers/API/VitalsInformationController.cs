﻿using AmbuTrans.DataLayer.Repository;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AmbuTrans.Controllers
{
    [RoutePrefix("API/VitalsInformation")]
    public class VitalsInformationController : ApiController
    {
        private readonly VitalsInformationRepository _repo;
        public VitalsInformationController()
        {
            _repo = new VitalsInformationRepository();
        }
        [HttpPost]
        [Route("AddUpdate")]
        public ResponseModel<object> AddUpdateVitals(AddVitalsInformationModel model)
        {
            return _repo.AddUpdateVitalsInformation(model);
        }
        [HttpPost]
        [Route("Update")]
        public ResponseModel<object> UpdateVitalsInformation(EditVitalsInformationModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = _repo.UpdateDetails(model);
            return mResult;
        }
        [HttpGet]
        [Route("SelectedVitalsInformation")]
        public ResponseModel<EditVitalsInformationModel> SelectedVitalsInformation(int MedicalTransferID)
        {
            ResponseModel<EditVitalsInformationModel> mResult = new ResponseModel<EditVitalsInformationModel>();
            mResult = _repo.FindById(MedicalTransferID);
            return mResult;
        }
    }
}
