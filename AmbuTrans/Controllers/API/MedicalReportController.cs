﻿using AmbuTrans.DataLayer.Repository;
using AmbuTrans.Helper;
using AmbuTrans.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace AmbuTrans.Controllers
{
    [RoutePrefix("API/Report")]
    public class MedicalReportController : ApiController
    {
        private readonly MedicalReportRepository _repo;
        public MedicalReportController()
        {
            _repo = new MedicalReportRepository();
        }

        #region Add Medical Report
        [HttpPost]
        [Route("Add")]
        public async Task<ResponseModel<object>> AddMedicalReports()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }
            //try
            {
                StringBuilder modelString = new StringBuilder();
                Dictionary<string, object> dicFormData = new Dictionary<string, object>();

                var provider = await Request.Content.ReadAsMultipartAsync<InMemoryMultipartFormDataStreamProvider>(new InMemoryMultipartFormDataStreamProvider());

                //access form data
                NameValueCollection formData = provider.FormData;

                //access files
                IList<HttpContent> files = provider.Files;

                // key-value pairs.
                foreach (var key in formData.AllKeys)
                {
                    if (!dicFormData.Keys.Contains(key))
                        dicFormData.Add(key, string.Empty);
                    foreach (var val in formData.GetValues(key))
                    {
                        dicFormData[key] = val;
                        modelString.Append(string.Format("{0}: {1}", key, val));
                    }
                }
                string Json = Newtonsoft.Json.JsonConvert.SerializeObject(dicFormData);
                AddMedicalReport model = JsonConvert.DeserializeObject<AddMedicalReport>(Json);

                #region save uploaded Report

                string filename = String.Empty;
                List<string> NameOfFile = new List<string>();
                List<AddMedicalReport> ReportDetails = new List<AddMedicalReport>();
                foreach (HttpContent file in files)
                {
                    //to append any text in filename.
                    string ogFileName = file.Headers.ContentDisposition.FileName.Trim('\"');
                    string shortGuid = ShortGuid.NewGuid().ToString();
                    var thisFileName = DateTime.Now.ToString("yyyyMMddHHmmssfff_") + shortGuid + Path.GetExtension(ogFileName);

                    Stream input = await file.ReadAsStreamAsync();
                    byte[] filearray = await file.ReadAsByteArrayAsync();
                    filename = Path.Combine(HttpContext.Current.Server.MapPath("~/Files/MedicalReports"), thisFileName);
                    NameOfFile.Add(thisFileName);
                    using (Stream fileStream = File.OpenWrite(filename))
                    {
                        input.CopyTo(fileStream);
                        //close file
                        fileStream.Close();
                    }
                }
                model.ReportFileName = NameOfFile;
                #endregion save uploaded images

                mResult = _repo.AddMedicalReport_(model);
            }
            //catch (Exception err)
            //{
            //    mResult.Message = err.Message;
            //    mResult.Status = ResponseStatus.Failed;
            //    mResult.Result = err;
            //}
            return mResult;
        }

        #endregion

        #region All Medical Report
        [HttpGet]
        [Route("AllMedicalReportList")]
        public ResponseModel<List<ReportDetailsModel>> ReportList(int DoctorID)
        {
            ResponseModel<List<ReportDetailsModel>> mResult = new ResponseModel<List<ReportDetailsModel>>();
            mResult = _repo.GetAllReports(DoctorID);
            if (mResult.Result.Count > 0)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = mResult.Result.Count + " Record listed.";
            }
            else
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "No record listed.";
            }
            return mResult;
        }
        #endregion.

        #region SelectedMedicalReport
        [HttpGet]
        [Route("SelectedMedicalReport")]
        public ResponseModel<List<ViewMedicalReport>> MedicalReport(int MedicalTransferID)
        {
            ResponseModel<List<ViewMedicalReport>> mResult = new ResponseModel<List<ViewMedicalReport>>();
            mResult.Result = _repo.GetReports(MedicalTransferID).ToList();
            if (mResult.Result.Count > 0)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = mResult.Result.Count + " Record listed.";
            }
            else
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "No record listed.";
            }
            return mResult;
        }
        #endregion

        #region SelectedMedicalReport Delete
        [HttpGet]
        [Route("Delete")]
        public ResponseModel<object> DeleteMedicalReport(int ReportFileID)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            bool result= _repo.DeleteReport(ReportFileID);
            if (result)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Report Deleted.";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Report not found.";
            }
         
            return mResult;
        }
        #endregion
    }

}
