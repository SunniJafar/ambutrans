﻿using AmbuTrans.DataLayer.Repository;
using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AmbuTrans.Controllers
{
    [RoutePrefix("API")]
    public class UtilityController : ApiController
    {
        private readonly UtilityRepository _repo;
        public UtilityController()
        {
            _repo = new UtilityRepository();
        }
        [HttpGet]
        [Route("MasterApiData")]
        public ResponseModel<MasterApiModel> ListofData()
        {
            ResponseModel<MasterApiModel> mResult = new ResponseModel<MasterApiModel>();
            mResult = _repo.GetAllApiData();
            if (mResult.Result!=null)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message =" Record listed.";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "No record listed.";
            }
            return mResult;
        }
        [HttpGet]
        [Route("AboutUs")]
        public ResponseModel<AboutUsModel> AboutUs()
        {
            ResponseModel<AboutUsModel> mResult = new ResponseModel<AboutUsModel>();
            mResult = _repo.GetAboutUs();
            if (mResult.Result != null)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = " Record listed.";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "No record listed.";
            }
            return mResult;
        }

        [HttpGet]
        [Route("TermsandConditions")]
        public ResponseModel<TermModel> TermsandConditions()
        {
            ResponseModel<TermModel> mResult = new ResponseModel<TermModel>();
            mResult = _repo.TermsandConditions();
            if (mResult.Result != null)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = " Record listed.";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "No record listed.";
            }
            return mResult;
        }

        [HttpGet]
        [Route("RecomdationSubcategoryList")]
        public ResponseModel<List<RecomdationSubcategoryModel>> ListSubategory(int RecomdationID)
        {
            ResponseModel<List<RecomdationSubcategoryModel>> mResult = new ResponseModel<List<RecomdationSubcategoryModel>>();
            mResult.Result = _repo.GetSubcategory(RecomdationID).ToList();
            if (mResult.Result.Count > 0)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = mResult.Result.Count + " Record listed.";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "No record listed.";
            }
            return mResult;
        }
        [HttpGet]
        [Route("ViewFormReportStatus")]
        public ResponseModel<List<ViewViewForm_ReportStatusModel>> ViewFormReportStatus(int MedicalTransferID)
        {
            ResponseModel<List<ViewViewForm_ReportStatusModel>> mResult = new ResponseModel<List<ViewViewForm_ReportStatusModel>>();
            mResult.Result = _repo.GetAllFormStatus(MedicalTransferID).ToList();
            if (mResult.Result.Count > 0)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = mResult.Result.Count + " Record listed.";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "No record listed.";
            }
            return mResult;

        }
    }
}
