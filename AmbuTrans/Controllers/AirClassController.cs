﻿using AmbuTrans.DataLayer.Repository;
using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AmbuTrans.Controllers
{
    public class AirClassController : Controller
    {
        private readonly AirClassRepository _repo;
        public AirClassController()
        {
            _repo = new AirClassRepository();
        }
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                ViewBag.breadcrumbModel = new BreadcrumbModel() { subActionTitle = "View AirClass" };
                IQueryable<AirClassModel> List = _repo.GetAirClassList();
                return View(List);
            }
            else
                return RedirectToAction("Login", "AdminAccount");
        }
        [HttpGet]
        public ActionResult Add()
        {
            if (Session["UserID"] != null)
            {
                ViewBag.Action = "Add";
                ViewBag.breadcrumbModel = new BreadcrumbModel()
                {
                    actioncontroller = "AirClass",
                    actionName = "Index",
                    actionTitle = "AirClass",
                    subActionTitle = "Add"
                };
                return View("AddorUpdate");
            }
            else
                return RedirectToAction("Login", "AdminAccount");

        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            AddUpdateAirClassModel model = _repo.FindById(Id);

            if (model != null)
            {
                ViewBag.Action = "Edit";
                ViewBag.breadcrumbModel = new BreadcrumbModel()
                {
                    actioncontroller = "AirClass",
                    actionName = "Index",
                    actionTitle = "AirClass",
                    subActionTitle = "Edit"
                };
                return View("AddorUpdate", model);
            }
            return Redirect(Url.Action("Index"));
        }
        [HttpPost]
        public ActionResult Edit(AddUpdateAirClassModel model)
        {
            if (ModelState.IsValid)
            {

                ResponseModel<AddUpdateAirClassModel> mResult = new ResponseModel<AddUpdateAirClassModel>();
                bool obj = Convert.ToBoolean(_repo.AddorUpdate(model));
                if (obj == true)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Record Updated successfully.";
                }
                TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                {
                    return Redirect(Url.Action("Index"));
                }
            }
            ViewBag.Action = "Edit";
            ViewBag.breadcrumbModel = new BreadcrumbModel()
            {
                actioncontroller = "AirClass",
                actionName = "Index",
                actionTitle = "AirClass",
                subActionTitle = "Edit"
            };

            return View("AddorUpdate", model);
        }

        [HttpPost]
        public ActionResult Add(AddUpdateAirClassModel model)
        {
            if (ModelState.IsValid)
            {
                ResponseModel<object> mResult = new ResponseModel<object>();
                bool obj = Convert.ToBoolean(_repo.AddorUpdate(model));
                if (obj == true)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Record add successfully.";
                }
                TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                {
                    return RedirectToAction("Index");
                }
            }
            ViewBag.Action = "Add";
            ViewBag.breadcrumbModel = new BreadcrumbModel()
            {
                actioncontroller = "AirClass",
                actionName = "Index",
                actionTitle = "AirClass",
                subActionTitle = "Add"
            };

            return View("AddorUpdate", model);
        }

        public JsonResult Delete(int Id)
        {
            ResponseModel<object> mResult = _repo.Delete(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }
    }
}