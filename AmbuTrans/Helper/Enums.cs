﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmbuTrans.Helper
{
    public enum ResponseStatus
    {
        Failed = 0,
        Success = 1,
        CustomerExist = 2,
        IsNotActive=3
        
    }
    public enum AlertStatus
    {
        danger = 0,
        success = 1,
        info = 2,
        warning = 3,
        primary = 4
    }
    public enum Station
    {
        ICU =0,
        Normal_Station=1
    }
    public enum NotificationStatus
    {
        Admin_Notification = 0,
        Message_Receive = 1,
        Inquiry = 2
    }
    public enum NotificationPageTypes
    {
        None = 0,
        HomePage = 1,
        JobCenter = 2,
        Notification = 3,
        Messages = 4,
        MyProfile = 5,
        SpecialOffers = 6,
        ContractorSearch = 7,
        Saved = 8,
        MyConnections = 9,
        Referral = 10,
        Statistics = 11,
        AddPortfolio = 12,
        TermsCondition = 13,
        PrivacyPolicy = 14,
        Logout = 15,
    }
    public enum Role
    {
        Admin = 0, // for admin
        Doctor = 1, // for Custumer user
    }
}