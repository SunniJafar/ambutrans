﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace AmbuTrans.Helper
{
    public class GlobalConfig
    {
        public const string ProjectName = "Ambu-Trans";
        public static bool isLive = Convert.ToBoolean(ConfigurationManager.AppSettings["isLive"].ToString());
        public const string AdminEmailID = "";
        public const string defaultImageName = "default.png";
        public static string DateFormat = "MMM dd,yyyy";


        public const string ToMailID = "";
        public const string EmailUserName = "notify.ambutrans@gmail.com";
        public const string EmailPassword = "ambutrans@123";
        public const string SMTPClient = "smtp.gmail.com";
        public const string Server = "relay-hosting.secureserver.net";

        public static string baseRoutePath = isLive ? ConfigurationManager.AppSettings["liveBaseRoutePath"] : ConfigurationManager.AppSettings["localBaseRoutePath"];
        public static string fileRoutePath = baseRoutePath + @"Files\";
        public static string ReportFilePath = fileRoutePath + @"MedicalReports\";
        public static string SignaturePath = fileRoutePath + @"SignatureImages\";

        public static string baseUrl = "http://ambutrans.imperoserver.in/";
        public static string ReportPath = "http://ambutrans.imperoserver.in/Files/MedicalReports/";


        public const int defaultLanguageId = 1;

        public static string SenderID = "742225539125";
        public const int NotificationPageSize = 1000;
        public const int PageSize = 10;
        public static string GoogleServerKey = "AAAArNAW8DU:APA91bFtRxAr13-3n5OhacntH_LUzwK7oZ7BFvQLb40AJGD65J6VUNotWN4zcGIqImnRS7lV4kn02p8DRpd54jig42jPVm7ZMcuaWSna48NQPZuwH4uOUCCzVUVbEom28FXrl8qvzj-Z";
        public static string GoogleApplicationID = "AIzaSyAmydiRyAoE8iLW1uY7-UqsSusF_AYngWk";
    }
}