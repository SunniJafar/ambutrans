﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace AmbuTrans.Helper
{
    public class NotificationPayload
    {
        public NotificationPayload()
        {
            content_available = 0;
        }
        /// <summary>
        /// The array must contain at least 1 and at most 1000 registration tokens.
        /// </summary>
        public string[] registration_ids { get; set; }

        /// <summary>
        /// When a notification is sent and this is set to true, an inactive client app is awoken. 
        /// </summary>
        public int content_available { get; set; }

        /// <summary>
        /// This parameter specifies the predefined, user-visible key-value pairs of the notification payload.
        /// </summary>
        public NotificationOptionModel notification { get; set; }

        /// <summary>
        /// This parameter specifies the custom key-value pairs of the message's payload.
        /// </summary>
        public CustomDataModel data { get; set; }

    }

    public class NotificationOptionModel
    {
        public NotificationOptionModel()
        {
            title = GlobalConfig.ProjectName;
            sound = "default";
            badge = 0;
            icon = "ic_notification_icon";
            body = string.Empty;
        }
        /// <summary>
        /// The notification's title. **This field is not visible on iOS phones and tablets.
        /// </summary>
        public string title { get; set; }

        /// <summary>
        /// The notification's body text.
        /// </summary>
        public string body { get; set; }

        /// <summary>
        /// The sound to play when the device receives the notification.
        /// </summary>
        public string sound { get; set; }

        /// <summary>
        /// The value of the badge on the home screen app icon. 
        /// If not specified, the badge is not changed.
        /// If set to 0, the badge is removed.
        /// </summary>
        public int badge { get; set; }

        /// <summary>
        /// The notification's icon.
        /// Sets the notification icon to myicon for drawable resource myicon. If you don't send this key in the request, FCM displays the launcher icon specified in your app manifest.
        /// </summary>
        public string icon { get; set; }

    }

    public class CustomDataModel
    {
        public NotificationStatus NotificationStatus { get; set; }

        public string TablePrimaryID { get; set; }

        public Role Role { get; set; }
    }

    public class PushNotification
    {
        //public ResponseModel<object> SendNotification(NotificationSendModel model)
        //{
        //    NotificationPayload notificationPayload = new NotificationPayload()
        //    {
        //        registration_ids = model.DeviceTokenList.Select(x => x.DeviceToken).ToArray(),
        //        notification = new NotificationOptionModel()
        //        {
        //            body = model.NotificationText,
        //        },
        //        data = new CustomDataModel()
        //        {
        //            NotificationStatus = model.NotificationStatus,
        //            TablePrimaryID = model.TablePrimaryID,
        //            Role = model.SenderRole,
        //        },

        //    };

        //    ResponseModel<object> mResult = new ResponseModel<object>();
        //    mResult.Result = SendNotification(notificationPayload);


        //    mResult.Status = ResponseStatus.Success;
        //    return mResult;
        //}

        public string SendNotification(NotificationPayload model)
        {
            string sResponseFromServer = string.Empty;
            string senderId = GlobalConfig.SenderID;
            WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = "application/json";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", GlobalConfig.GoogleApplicationID));
            tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

            const int pageSize = GlobalConfig.NotificationPageSize;

            string[] DeviceTokenList = model.registration_ids;
            int currentPage = 0;
            for (int i = 0; i < DeviceTokenList.Count(); i = i + pageSize)
            {
                model.registration_ids = (DeviceTokenList.Skip(currentPage * pageSize).Take(pageSize)).ToArray();

                string jsonResult = JsonConvert.SerializeObject(model);

                if (!string.IsNullOrEmpty(jsonResult))
                {
                    jsonResult = jsonResult.Replace("content_available", "content-available");
                    Byte[] byteArray = Encoding.UTF8.GetBytes(jsonResult);
                    tRequest.ContentLength = byteArray.Length;

                    try
                    {
                        using (Stream dataStream = tRequest.GetRequestStream())
                        {
                            dataStream.Write(byteArray, 0, byteArray.Length);

                            using (WebResponse tResponse = tRequest.GetResponse())
                            {
                                using (Stream dataStreamResponse = tResponse.GetResponseStream())
                                {
                                    using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                    {
                                        sResponseFromServer = tReader.ReadToEnd();
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception err)
                    {
                        sResponseFromServer = err.Message;
                    }
                }
            }

            return sResponseFromServer;
        }
    }
}