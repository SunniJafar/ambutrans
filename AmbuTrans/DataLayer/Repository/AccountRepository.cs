﻿using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Net.Mail;

namespace AmbuTrans.DataLayer.Repository
{
    public class AccountRepository : IDisposable
    {
        AmbuTransEntities _ctx;
        public AccountRepository()
        {
            _ctx = new AmbuTransEntities();
        }
        public void Dispose()
        {
            _ctx.Dispose();
        }
        public bool ChangePassword(ChangePasswordModel model)
        {
            DoctorMaster result = _ctx.DoctorMasters.SingleOrDefault(a => a.EmailID == model.EmailID && a.Password == model.OldPassword);
            if (result != null)
            {
                result.Password = model.NewPassword;
                _ctx.SaveChanges();
                return true;
            }
            return false;
        }

        public ResponseModel<LoginModel> UserLogin(LoginModel model)
        {
            ResponseModel<LoginModel> mResult = new ResponseModel<LoginModel>();
            DoctorMaster result = _ctx.DoctorMasters.FirstOrDefault(x => x.EmailID == model.EmailID && x.Password == model.Password);
            if (result != null)
            {
                DoctorMaster status = _ctx.DoctorMasters.FirstOrDefault(x => x.EmailID == model.EmailID && x.IsActive == true);
                if (status != null)
                {
                    result.DeviceToken = model.DeviceToken;
                    result.Platform = model.Platform;
                    result.LoginStatus = true;

                    _ctx.SaveChanges();
                    mResult.Result = new RegisterModel()
                    {
                        DoctorName = result.DoctorName,
                        EmailID = result.EmailID,
                        Password = result.Password,
                        DeviceToken = result.DeviceToken,
                        Platform = result.Platform,
                        DoctorID = result.DoctorID
                    };
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "User Logged in successfully.";
                }
                else
                {
                    mResult.Status = ResponseStatus.IsNotActive;
                    mResult.Message = "Kindly please contact the Administrator to confirm your registration.";
                }
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Invalid Username or Password.";
            }

            return mResult;
        }
        public ResponseModel<object> AddUser(RegisterModel model)
        {

            ResponseModel<object> mResult = new ResponseModel<object>();
            DoctorMaster user = _ctx.DoctorMasters.Where(x => x.EmailID == model.EmailID).FirstOrDefault();
            if (user == null)
            {
                DoctorMaster Reg = new DoctorMaster();
                Reg.DoctorName = model.DoctorName;
                Reg.EmailID = model.EmailID;
                Reg.Password = model.Password;
                Reg.DeviceToken = model.DeviceToken;
                Reg.Platform = model.Platform;
                Reg.IsActive = false;
                Reg.AddedDate = System.DateTime.Now;

                _ctx.DoctorMasters.Add(Reg);
                _ctx.SaveChanges();

                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Kindly please contact the Administrator to confirm your registration.";
                model.DoctorID = Reg.DoctorID;
                mResult.Result = model;
            }
            else
            {
                mResult.Status = ResponseStatus.CustomerExist;
                mResult.Message = "EmailID already exists";
            }
            return mResult;
        }
        public bool Logout(int DoctorID)
        {
            DoctorMaster User = _ctx.DoctorMasters.SingleOrDefault(a => a.DoctorID == DoctorID);
            if (User != null)
            {
                User.LoginStatus = false;
                _ctx.SaveChanges();
                return true;
            }
            else
                return false;
        }
        public bool ForgetUserPassword(ForgetPasswordModel model)
        {
            DoctorMaster User = _ctx.DoctorMasters.SingleOrDefault(a => a.EmailID == model.EmailID);
            if (User != null)
            {
                {
                    MailMessage mailmessage = new MailMessage();
                    SmtpClient smtpClient = new SmtpClient();
                    string EmailUserName = GlobalConfig.EmailUserName;
                    string EmailPassword = GlobalConfig.EmailPassword;
                    string SMTPClient = GlobalConfig.SMTPClient;

                    MailAddress fromAddress = new MailAddress(EmailUserName);
                    mailmessage.From = fromAddress;

                    mailmessage.To.Add(model.EmailID);
                    mailmessage.Subject = "Ambu Trans Forget Your Password";
                    mailmessage.IsBodyHtml = true;
                    mailmessage.Body = PopulateMailBody(User.DoctorName, User.EmailID, User.Password);

                    smtpClient.Host = SMTPClient;
                    smtpClient.Port = 25;//25 localhost
                    smtpClient.EnableSsl = true;
                    smtpClient.UseDefaultCredentials = true;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.Credentials = new NetworkCredential(EmailUserName, EmailPassword);

                    smtpClient.Send(mailmessage);
                }
               
                return true;
            }
            else
                return false;
        }
        private string PopulateMailBody(string Name, string Email, string password)
        {
            string body = string.Empty;
            string projectName = "Property Turnover";
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Files/EmailTemplate/ForgetPassword.htm")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{projectName}", projectName);
            body = body.Replace("{Name}", Name);
            body = body.Replace("{UserName}", Email);
            body = body.Replace("{Password}", password);
            return body;
        }
    }
}