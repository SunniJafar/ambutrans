﻿using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmbuTrans.DataLayer.Repository
{
    public class AirClassRepository
    {
        #region Connection
        AmbuTransEntities _ctx;
        public AirClassRepository()
        {
            _ctx = new AmbuTransEntities();
        }
        public void Dispose()
        {
            _ctx.Dispose();
        }
        #endregion
        public IQueryable<AirClassModel> GetAirClassList()
        {
            return (from x in _ctx.ClassMasters
                    orderby x.ClassName ascending
                    select new AirClassModel
                    {
                        ClassID = x.ClassID,
                        ClassName = x.ClassName
                    });
        }
        public bool AddorUpdate(AddUpdateAirClassModel model)
        {
            if (model.ClassID != 0)
            {
                ClassMaster c = _ctx.ClassMasters.SingleOrDefault(a => a.ClassID == model.ClassID);
                if (c != null)
                {
                    c.ClassName = model.ClassName;
                    _ctx.SaveChanges();
                    return true;
                }
            }
            else
            {
                ClassMaster result = new ClassMaster();
                result.ClassName = model.ClassName;
                _ctx.ClassMasters.Add(result);
                _ctx.SaveChanges();
                return true;

            }
            return false;
        }
        public AddUpdateAirClassModel FindById(int Id)
        {
            AddUpdateAirClassModel e = (from x in _ctx.ClassMasters
                                           where x.ClassID == Id
                                           select new AddUpdateAirClassModel
                                           {
                                               ClassID = x.ClassID,
                                               ClassName = x.ClassName,
                                           }).FirstOrDefault();
            return e;

        }
        public ResponseModel<object> Delete(int Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            ClassMaster e = _ctx.ClassMasters.FirstOrDefault(x => x.ClassID == Id);
            if (e != null)
            {
                _ctx.ClassMasters.Remove(e);
                _ctx.SaveChanges();
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Record details deleted.";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Record not found.";
            }
            return mResult;
        }
    }
}