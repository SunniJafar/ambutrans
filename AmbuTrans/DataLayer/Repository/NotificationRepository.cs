﻿using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmbuTrans.DataLayer.Repository
{
    public class NotificationRepository : IDisposable
    {
        AmbuTransEntities _ctx;
        PushNotification _pushNotification;
        public NotificationRepository()
        {
            _ctx = new AmbuTransEntities();
            _pushNotification = new PushNotification();
        }
        public void Dispose()
        {
            _ctx.Dispose();
        }
        public ResponseModel<List<ViewNotificationModel>> GetMessage(int DoctorID)
        {
            ResponseModel<List<ViewNotificationModel>> mResult = new ResponseModel<List<ViewNotificationModel>>();
            DoctorMaster doctor = _ctx.DoctorMasters.Where(x => x.DoctorID == DoctorID).FirstOrDefault();
            DateTime dateTime = doctor.AddedDate.Date;
            mResult.Result = (from a in _ctx.NotificationMasters
                              where a.DateTIme>dateTime
                              orderby a.NotificationID descending
                              select new
                              {
                                  a.NotificationID,
                                  a.Message,
                                  a.DateTIme,
                                  
                              }).Select(a => new ViewNotificationModel
                              {
                                  NotificationID = a.NotificationID,
                                  Message = a.Message,
                                  AddedDate = a.DateTIme
                              }).ToList();
            List<ViewNotificationModel> list = new List<ViewNotificationModel>();
            foreach (var item in mResult.Result)
            {
                list.Add(new ViewNotificationModel
                {
                    NotificationID = item.NotificationID,
                    Message = item.Message,
                    Date = item.AddedDate.ToString("dd/mm/yyyy"),
                    Time = TimeAgo(item.AddedDate),
                });
            }
            mResult.Result = list;
            return mResult;

        }
        public static string TimeAgo(DateTime dt)
        {
            TimeSpan span = DateTime.Now - dt;
            if (span.Days > 365)
            {
                int years = (span.Days / 365);
                if (span.Days % 365 != 0)
                    years += 1;
                return String.Format("{0} {1} ago",
                years, years == 1 ? "year" : "years");
            }
            if (span.Days > 30)
            {
                int months = (span.Days / 30);
                if (span.Days % 31 != 0)
                    months += 1;
                return String.Format("{0} {1} ago",
                months, months == 1 ? "month" : "months");
            }
            if (span.Days > 0)
                return String.Format("{0} {1} ago",
                span.Days, span.Days == 1 ? "day" : "days");
            if (span.Hours > 0)
                return String.Format("{0} {1} ago",
                span.Hours, span.Hours == 1 ? "Hrs" : "hours");
            if (span.Minutes > 0)
                return String.Format("{0} {1} ago",
                span.Minutes, span.Minutes == 1 ? "minute" : "minutes");
            if (span.Seconds > 5)
                return String.Format("{0} seconds ago", span.Seconds);
            if (span.Seconds <= 5)
                return "just now";
            return string.Empty;
        }

        public bool SendMessage(SendMessageModel model)
        {
            NotificationMaster master = new NotificationMaster();
            master.Message = model.Message;
            master.DateTIme = DateTime.Now;
            _ctx.NotificationMasters.Add(master);
            _ctx.SaveChanges();

            string[] DeviceTokenList = (from a in _ctx.DoctorMasters
                                        where a.LoginStatus==true
                                        select a.DeviceToken).ToArray();

            if (DeviceTokenList.Count() > 0)
            {
                NotificationPayload payload = new NotificationPayload()
                {
                    registration_ids = DeviceTokenList,
                    notification = new NotificationOptionModel()
                    {
                        body = model.Message,
                    },
                    data = new CustomDataModel()
                    {
                        NotificationStatus = NotificationStatus.Admin_Notification,
                        Role = Role.Admin,
                        //TablePrimaryID = inquiry.inquiryid.tostring(),
                    },
                };
                _pushNotification.SendNotification(payload);
            }
            return true;
        }

        public IQueryable<ViewMessageModel> GetMessageList()
        {
            return (from x in _ctx.NotificationMasters
                    orderby x.NotificationID descending
                    select new ViewMessageModel
                    {
                        MessageID=x.NotificationID,
                        Message = x.Message,
                        DateTIme = x.DateTIme
                    });
        }

        public ResponseModel<object> Delete(int Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            NotificationMaster e = _ctx.NotificationMasters.FirstOrDefault(x => x.NotificationID == Id);
            if (e != null)
            {
                _ctx.NotificationMasters.Remove(e);
                _ctx.SaveChanges();
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Message deleted.";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Message not found.";
            }
            return mResult;
        }
    }
}