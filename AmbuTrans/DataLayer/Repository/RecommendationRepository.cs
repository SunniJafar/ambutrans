﻿using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmbuTrans.DataLayer.Repository
{

    public class RecommendationRepository
    {
        #region Connection
        AmbuTransEntities _ctx;
        public RecommendationRepository()
        {
            _ctx = new AmbuTransEntities();
        }
        public void Dispose()
        {
            _ctx.Dispose();
        }
        #endregion
        public IQueryable<RecomdationCategoryModel> GetRecomdationList()
        {
            return (from x in _ctx.RecomdationCategories
                    orderby x.RecomdationID ascending
                    select new RecomdationCategoryModel
                    {
                        RecomdationID = x.RecomdationID,
                        RecomdationName = x.RecomdationName
                    });
        }
        public bool AddorUpdate(AddUpdateRecomdationModel model)
        {
            if (model.RecomdationID != 0)
            {
                RecomdationCategory c = _ctx.RecomdationCategories.SingleOrDefault(a => a.RecomdationID == model.RecomdationID);
                if (c != null)
                {
                    c.RecomdationName = model.RecomdationName;
                    _ctx.SaveChanges();
                    return true;
                }
            }
            else
            {
                RecomdationCategory result = new RecomdationCategory();
                result.RecomdationName = model.RecomdationName;
                _ctx.RecomdationCategories.Add(result);
                _ctx.SaveChanges();
                return true;

            }
            return false;
        }
        public AddUpdateRecomdationModel FindById(int Id)
        {
            AddUpdateRecomdationModel e = (from x in _ctx.RecomdationCategories
                                   where x.RecomdationID == Id
                                   select new AddUpdateRecomdationModel
                                   {
                                       RecomdationID = x.RecomdationID,
                                       RecomdationName = x.RecomdationName,
                                   }).FirstOrDefault();
            return e;

        }
        public ResponseModel<object> Delete(int Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            RecomdationCategory e = _ctx.RecomdationCategories.FirstOrDefault(x => x.RecomdationID == Id);
            if (e != null)
            {
                _ctx.RecomdationCategories.Remove(e);
                _ctx.SaveChanges();
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Record details deleted.";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Record not found.";
            }
            return mResult;
        }
    }
}