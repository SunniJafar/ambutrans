﻿using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace AmbuTrans.DataLayer.Repository
{
    public class MedicalTransferRepository : IDisposable
    {
        AmbuTransEntities _ctx;
        public MedicalTransferRepository()
        {
            _ctx = new AmbuTransEntities();
        }
        public void Dispose()
        {
            _ctx.Dispose();
        }
        public ResponseModel<object> AddUpdateMedicalTransfer(AddMedicalTransferModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                if (model.MedicalTransferID != 0)
                {
                    MedicalTransferMaster result = _ctx.MedicalTransferMasters.FirstOrDefault(x => x.MedicalTransferID == model.MedicalTransferID);
                    if (result != null)
                    {
                        if (model.SignatureImageName != null)
                        {
                            string filepath = HttpContext.Current.Server.MapPath("/Files/SignatureImages/") + result.SignatureImageName;
                            if ((System.IO.File.Exists(filepath)))
                            {
                                System.IO.File.Delete(filepath);
                            }
                        }
                        result.NameOfPatient = model.NameOfPatient;
                        result.ToWhomItMayConcern = model.ToWhomItMayConcern;
                        result.Age = model.Age;
                        result.Gender = model.Gender;
                        result.DateOfBirth = model.DateOfBirth;
                        result.PresentedInHospital = model.PresentedInHospital;
                        result.HospitalAddress = model.HospitalAddress;
                        result.Date = model.Date;
                        //result.RepatriationNecessaryBecauseOf = model.RepatriationNecessaryBecauseOf;
                        result.HospitalizedTo = model.HospitalizedTo;
                        result.ContactPerson = model.ContactPerson;
                        result.ContactPersonAddress = model.ContactPersonAddress;
                        result.BedArranged = model.BedArranged;
                        result.TransferArrivalDate = model.TransferArrivalDate;
                        result.Time = model.Time;
                        result.ContactPersonFamily = model.ContactPersonFamily;
                        result.PhoneNumber = model.PhoneNumber;
                        result.MedicalHistory = model.MedicalHistory;
                        result.Diagnosis = model.Diagnosis;
                        result.Treatment = model.Treatment;
                        result.FitToFlyDate = model.FitToFlyDate;
                        result.ClassID = model.ClassID;
                        result.Airline = model.Airline;
                        result.FlightHrs = model.FlightHrs;
                        result.SpecialRequest = model.SpecialRequest;
                        result.MedcrewAmbulanceAT = model.MedcrewAmbulanceAT;
                        result.DoctorName = model.DoctorName;
                        result.SignatureImageName = model.SignatureImageName;
                        result.DoctorID = model.DoctorID;
                        result.Driver = model.Driver;
                        result.Paramedic = model.Paramedic;
                        _ctx.SaveChanges();

                        _ctx.RepatriationNecessaryTransactions.RemoveRange(_ctx.RepatriationNecessaryTransactions.Where(c => c.MedicalTransferID == model.MedicalTransferID));
                        _ctx.SaveChanges();

                        List<RepatriationNecessaryTransaction> RepatriationNecessary = new List<RepatriationNecessaryTransaction>();
                        if (model.RepatriationNecessaryBecauseOf != null)
                        {
                            foreach (var item in model.RepatriationNecessaryBecauseOf)
                            {
                                RepatriationNecessary.Add(new RepatriationNecessaryTransaction
                                {
                                    MedicalTransferID = result.MedicalTransferID,
                                    RepatriationID = item.RepatriationID
                                });
                            }
                            _ctx.RepatriationNecessaryTransactions.AddRange(RepatriationNecessary);
                            _ctx.SaveChanges();
                        }

                        _ctx.RecomdationSubcategoryTransactions.RemoveRange(_ctx.RecomdationSubcategoryTransactions.Where(c => c.MedicalTransferID == model.MedicalTransferID));
                        _ctx.SaveChanges();

                        List<RecomdationSubcategoryTransaction> transactionList = new List<RecomdationSubcategoryTransaction>();
                        if (model.RecomdationSubcategoryTransaction != null)
                        {
                            foreach (var item in model.RecomdationSubcategoryTransaction)
                            {
                                transactionList.Add(new RecomdationSubcategoryTransaction
                                {
                                    MedicalTransferID = result.MedicalTransferID,
                                    RecomdationSubcategoryID = item.RecomdationSubcategoryID
                                });
                            }
                            _ctx.RecomdationSubcategoryTransactions.AddRange(transactionList);
                            _ctx.SaveChanges();
                        }
                        mResult.Result = model;
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = "Details Updated successfully";

                    }
                    else
                    {
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = "Details not Update";
                    }
                }
                else
                {
                    MedicalTransferMaster medicalTransfer = new MedicalTransferMaster();
                    medicalTransfer.RegisterDate = model.RegisterDate;
                    medicalTransfer.NameOfPatient = model.NameOfPatient;
                    medicalTransfer.ToWhomItMayConcern = model.ToWhomItMayConcern;
                    medicalTransfer.Age = model.Age;
                    medicalTransfer.Gender = model.Gender;
                    medicalTransfer.DateOfBirth = model.DateOfBirth;
                    medicalTransfer.PresentedInHospital = model.PresentedInHospital;
                    medicalTransfer.HospitalAddress = model.HospitalAddress;
                    medicalTransfer.Date = model.Date;
                    //medicalTransfer.RepatriationNecessaryBecauseOf = model.RepatriationNecessaryBecauseOf;
                    medicalTransfer.HospitalizedTo = model.HospitalizedTo;
                    medicalTransfer.ContactPerson = model.ContactPerson;
                    medicalTransfer.ContactPersonAddress = model.ContactPersonAddress;
                    medicalTransfer.BedArranged = model.BedArranged;
                    medicalTransfer.TransferArrivalDate = model.TransferArrivalDate;
                    medicalTransfer.Time = model.Time;
                    medicalTransfer.ContactPersonFamily = model.ContactPersonFamily;
                    medicalTransfer.PhoneNumber = model.PhoneNumber;
                    medicalTransfer.MedicalHistory = model.MedicalHistory;
                    medicalTransfer.Diagnosis = model.Diagnosis;
                    medicalTransfer.Treatment = model.Treatment;
                    medicalTransfer.FitToFlyDate = model.FitToFlyDate;
                    medicalTransfer.ClassID = model.ClassID;
                    medicalTransfer.Airline = model.Airline;
                    medicalTransfer.FlightHrs = model.FlightHrs;
                    medicalTransfer.SpecialRequest = model.SpecialRequest;
                    medicalTransfer.MedcrewAmbulanceAT = model.MedcrewAmbulanceAT;
                    medicalTransfer.DoctorName = model.DoctorName;
                    medicalTransfer.SignatureImageName = model.SignatureImageName;
                    medicalTransfer.DoctorID = model.DoctorID;
                    medicalTransfer.Paramedic = model.Paramedic;
                    medicalTransfer.Driver = model.Driver;
                    medicalTransfer.isActive = true;
                    medicalTransfer.isPdf = false;
                    _ctx.MedicalTransferMasters.Add(medicalTransfer);
                    _ctx.SaveChanges();

                    List<RepatriationNecessaryTransaction> RepatriationNecessary = new List<RepatriationNecessaryTransaction>();
                    if (model.RepatriationNecessaryBecauseOf != null)
                    {
                        foreach (var item in model.RepatriationNecessaryBecauseOf)
                        {
                            RepatriationNecessary.Add(new RepatriationNecessaryTransaction
                            {
                                MedicalTransferID = medicalTransfer.MedicalTransferID,
                                RepatriationID = item.RepatriationID
                            });
                        }
                        _ctx.RepatriationNecessaryTransactions.AddRange(RepatriationNecessary);
                        _ctx.SaveChanges();
                    }

                    List<RecomdationSubcategoryTransaction> transactionList = new List<RecomdationSubcategoryTransaction>();
                    if (model.RecomdationSubcategoryTransaction != null)
                    {
                        foreach (var item in model.RecomdationSubcategoryTransaction)
                        {
                            transactionList.Add(new RecomdationSubcategoryTransaction
                            {
                                MedicalTransferID = medicalTransfer.MedicalTransferID,
                                RecomdationSubcategoryID = item.RecomdationSubcategoryID
                            });
                        }
                        _ctx.RecomdationSubcategoryTransactions.AddRange(transactionList);
                        _ctx.SaveChanges();
                    }
                    model.MedicalTransferID = medicalTransfer.MedicalTransferID;
                    mResult.Result = model;
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Record submited.";
                }
            }

            catch (Exception err)
            {
                mResult.Result = null;
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = err.Message;
            }
            return mResult;
        }

        public ResponseModel<ViewMedicalTransferModel> FindById(int MedicalTransferID)
        {
            ResponseModel<ViewMedicalTransferModel> mResult = new ResponseModel<ViewMedicalTransferModel>();
            try
            {


                mResult.Result = (from x in _ctx.MedicalTransferMasters
                                  where x.MedicalTransferID == MedicalTransferID
                                  select new
                                  {
                                      x.MedicalTransferID,
                                      x.RegisterDate,
                                      x.NameOfPatient,
                                      x.ToWhomItMayConcern,
                                      x.Age,
                                      x.Gender,
                                      x.DateOfBirth,
                                      x.PresentedInHospital,
                                      x.HospitalAddress,
                                      x.Date,
                                      x.RepatriationNecessaryBecauseOf,
                                      x.HospitalizedTo,
                                      x.ContactPerson,
                                      x.ContactPersonAddress,
                                      x.BedArranged,
                                      x.TransferArrivalDate,
                                      x.Time,
                                      x.ContactPersonFamily,
                                      x.PhoneNumber,
                                      x.MedicalHistory,
                                      x.Diagnosis,
                                      x.Treatment,
                                      x.FitToFlyDate,
                                      x.ClassMaster,
                                      x.Airline,
                                      x.FlightHrs,
                                      x.SpecialRequest,
                                      x.MedcrewAmbulanceAT,
                                      x.DoctorName,
                                      x.ClassID,
                                      x.SignatureImageName,
                                      x.DoctorID,
                                      x.Paramedic,
                                      x.Driver,
                                  }).ToList().Select(x => new ViewMedicalTransferModel
                                  {
                                      MedicalTransferID = x.MedicalTransferID,
                                      RegisterDate = x.RegisterDate.ToString(GlobalConfig.DateFormat),
                                      NameOfPatient = x.NameOfPatient,
                                      ToWhomItMayConcern = x.ToWhomItMayConcern ?? string.Empty,
                                      Age = x.Age ?? 0,
                                      Gender = x.Gender,

                                      DateOfBirth = x.DateOfBirth?.ToString(GlobalConfig.DateFormat),

                                      PresentedInHospital = x.PresentedInHospital ?? string.Empty,

                                      HospitalAddress = x.HospitalAddress ?? string.Empty,
                                      Date = x.Date?.ToString(GlobalConfig.DateFormat),

                                      HospitalizedTo = x.HospitalizedTo ?? string.Empty,
                                      ContactPerson = x.ContactPerson ?? string.Empty,
                                      ContactPersonAddress = x.ContactPersonAddress ?? string.Empty,
                                      BedArranged = x.BedArranged,
                                      TransferArrivalDate = x.TransferArrivalDate?.ToString(GlobalConfig.DateFormat),
                                      Time = x.Time ?? string.Empty,
                                      ContactPersonFamily = x.ContactPersonFamily ?? string.Empty,
                                      PhoneNumber = x.PhoneNumber ?? string.Empty,
                                      MedicalHistory = x.MedicalHistory ?? string.Empty,
                                      Diagnosis = x.Diagnosis ?? string.Empty,
                                      Treatment = x.Treatment ?? string.Empty,
                                      FitToFlyDate = x.FitToFlyDate?.ToString(GlobalConfig.DateFormat),
                                      //ClassName = x.ClassMaster.ClassName ?? string.Empty,
                                      ClassID = x.ClassID,
                                      Airline = x.Airline,
                                      FlightHrs = x.FlightHrs ?? string.Empty,
                                      SpecialRequest = x.SpecialRequest ?? string.Empty,
                                      MedcrewAmbulanceAT = x.MedcrewAmbulanceAT ?? string.Empty,
                                      DoctorName = x.DoctorName ?? string.Empty,

                                      SignatureImageName = x.SignatureImageName != null ? GlobalConfig.baseUrl + "/Files/SignatureImages/" + x.SignatureImageName : string.Empty,

                                      DoctorID = x.DoctorID,
                                      Paramedic = x.Paramedic ?? string.Empty,
                                      Driver = x.Driver,
                                      RepatriationNecessaryBecauseOf = (from b in _ctx.RepatriationNecessaryTransactions
                                                                        where b.MedicalTransferID == MedicalTransferID
                                                                        select new RepatriationNecessaryTransactionModel
                                                                        {
                                                                            RepatriationNecessaryName = b.RepatriationNecessaryMaster.RepatriationName,
                                                                            RepatriationID = b.RepatriationID,
                                                                        }).ToList(),
                                      RecomdationSubcategoryTransaction = (from a in _ctx.RecomdationSubcategoryTransactions
                                                                           where a.MedicalTransferID == MedicalTransferID
                                                                           select new RecomdationSubTransactionModel
                                                                           {
                                                                               RecomdationSubName = a.RecomdationSubcategory.RecomdationSubcategoryName,
                                                                               RecomdationID = a.RecomdationSubcategory.RecomdationID,
                                                                               RecomdationSubcategoryID = a.RecomdationSubcategoryID


                                                                           }).ToList()
                                  }).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw;
            }
            if (mResult.Result != null)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Record listed.";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "No record listed.";
            }
            return mResult;
        }

        public ResponseModel<object> UpdateDetails(EditMedicalTransferModel model)
        {

            ResponseModel<object> mResult = new ResponseModel<object>();
            MedicalTransferMaster result = _ctx.MedicalTransferMasters.FirstOrDefault(x => x.MedicalTransferID == model.MedicalTransferID);
            if (result != null)
            {
                if (model.SignatureImageName != null)
                {
                    string filepath = HttpContext.Current.Server.MapPath("/Files/SignatureImages/") + result.SignatureImageName;
                    if ((System.IO.File.Exists(filepath)))
                    {
                        System.IO.File.Delete(filepath);
                    }
                }
                result.NameOfPatient = model.NameOfPatient;
                result.ToWhomItMayConcern = model.ToWhomItMayConcern;
                result.Age = model.Age;
                result.Gender = model.Gender;
                result.DateOfBirth = model.DateOfBirth;
                result.PresentedInHospital = model.PresentedInHospital;
                result.HospitalAddress = model.HospitalAddress;
                result.Date = model.Date;
                //result.RepatriationNecessaryBecauseOf = model.RepatriationNecessaryBecauseOf;
                result.HospitalizedTo = model.HospitalizedTo;
                result.ContactPerson = model.ContactPerson;
                result.ContactPersonAddress = model.ContactPersonAddress;
                result.BedArranged = model.BedArranged;
                result.TransferArrivalDate = model.TransferArrivalDate;
                result.Time = model.Time;
                result.ContactPersonFamily = model.ContactPersonFamily;
                result.PhoneNumber = model.PhoneNumber;
                result.MedicalHistory = model.MedicalHistory;
                result.Diagnosis = model.Diagnosis;
                result.Treatment = model.Treatment;
                result.FitToFlyDate = model.FitToFlyDate;
                result.ClassID = model.ClassID;
                result.Airline = model.Airline;
                result.FlightHrs = model.FlightHrs;
                result.SpecialRequest = model.SpecialRequest;
                result.MedcrewAmbulanceAT = model.MedcrewAmbulanceAT;
                result.DoctorName = model.DoctorName;
                result.SignatureImageName = model.SignatureImageName;
                result.DoctorID = model.DoctorID;
                result.Driver = model.Driver;
                result.Paramedic = model.Paramedic;
                _ctx.SaveChanges();

                _ctx.RepatriationNecessaryTransactions.RemoveRange(_ctx.RepatriationNecessaryTransactions.Where(c => c.MedicalTransferID == model.MedicalTransferID));
                _ctx.SaveChanges();

                List<RepatriationNecessaryTransaction> RepatriationNecessary = new List<RepatriationNecessaryTransaction>();
                if (model.RepatriationNecessaryBecauseOf != null)
                {
                    foreach (var item in model.RepatriationNecessaryBecauseOf)
                    {
                        RepatriationNecessary.Add(new RepatriationNecessaryTransaction
                        {
                            MedicalTransferID = result.MedicalTransferID,
                            RepatriationID = item.RepatriationID
                        });
                    }
                    _ctx.RepatriationNecessaryTransactions.AddRange(RepatriationNecessary);
                    _ctx.SaveChanges();
                }

                _ctx.RecomdationSubcategoryTransactions.RemoveRange(_ctx.RecomdationSubcategoryTransactions.Where(c => c.MedicalTransferID == model.MedicalTransferID));
                _ctx.SaveChanges();

                List<RecomdationSubcategoryTransaction> transactionList = new List<RecomdationSubcategoryTransaction>();
                if (model.RecomdationSubcategoryTransaction != null)
                {
                    foreach (var item in model.RecomdationSubcategoryTransaction)
                    {
                        transactionList.Add(new RecomdationSubcategoryTransaction
                        {
                            MedicalTransferID = item.MedicalTransferID,
                            RecomdationSubcategoryID = item.RecomdationSubcategoryID
                        });
                    }
                    _ctx.RecomdationSubcategoryTransactions.AddRange(transactionList);
                    _ctx.SaveChanges();
                }
                mResult.Result = model;
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Details Updated successfully";

            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Details not Update";
            }
            return mResult;
        }

        public ResponseModel<ViewMedicalTransferModel> ViewMedicalTransferReport(int MedicalTransferID)
        {
            ResponseModel<ViewMedicalTransferModel> mResult = new ResponseModel<ViewMedicalTransferModel>();
            try
            {

           
            mResult.Result = (from x in _ctx.MedicalTransferMasters
                              where x.MedicalTransferID == MedicalTransferID
                              select new
                              {
                                  x.MedicalTransferID,
                                  x.RegisterDate,
                                  x.NameOfPatient,
                                  x.ToWhomItMayConcern,
                                  x.Age,
                                  x.Gender,
                                  x.DateOfBirth,
                                  x.PresentedInHospital,
                                  x.HospitalAddress,
                                  x.Date,
                                  x.RepatriationNecessaryBecauseOf,
                                  x.HospitalizedTo,
                                  x.ContactPerson,
                                  x.ContactPersonAddress,
                                  x.BedArranged,
                                  x.TransferArrivalDate,
                                  x.Time,
                                  x.ContactPersonFamily,
                                  x.PhoneNumber,
                                  x.MedicalHistory,
                                  x.Diagnosis,
                                  x.Treatment,
                                  x.FitToFlyDate,
                                  x.ClassMaster,
                                  x.ClassID,
                                  x.Airline,
                                  x.FlightHrs,
                                  x.SpecialRequest,
                                  x.MedcrewAmbulanceAT,
                                  x.DoctorName,
                                  RecomdationName = x.RecomdationSubcategoryTransactions.FirstOrDefault(a => a.MedicalTransferID == x.MedicalTransferID).RecomdationSubcategory.RecomdationCategory.RecomdationName,
                                  x.SignatureImageName,
                                  x.DoctorID,
                                  x.Paramedic,
                                  x.Driver,
                                  RecomdationId = x.RecomdationSubcategoryTransactions.FirstOrDefault(a => a.MedicalTransferID == x.MedicalTransferID)!=null? x.RecomdationSubcategoryTransactions.FirstOrDefault(a => a.MedicalTransferID == x.MedicalTransferID).RecomdationSubcategory.RecomdationCategory.RecomdationID:1,
                              }).ToList().Select(a => new ViewMedicalTransferModel
                              {

                                  MedicalTransferID = a.MedicalTransferID,
                                  RegisterDate = a.RegisterDate.ToString(GlobalConfig.DateFormat),
                                  NameOfPatient = a.NameOfPatient,
                                  ToWhomItMayConcern = a.ToWhomItMayConcern,
                                  Age = a.Age,
                                  Gender = a.Gender,
                                  DateOfBirth = a.DateOfBirth?.ToString(GlobalConfig.DateFormat),
                                  PresentedInHospital = a.PresentedInHospital,
                                  HospitalAddress = a.HospitalAddress,
                                  Date = a.Date?.ToString(GlobalConfig.DateFormat),
                                  //RepatriationNecessaryBecauseOf = a.RepatriationNecessaryBecauseOf,
                                  HospitalizedTo = a.HospitalizedTo,
                                  ContactPerson = a.ContactPerson,
                                  ContactPersonAddress = a.ContactPersonAddress,
                                  BedArranged = a.BedArranged,
                                  TransferArrivalDate = a.TransferArrivalDate?.ToString(GlobalConfig.DateFormat),
                                  Time = a.Time,
                                  ContactPersonFamily = a.ContactPersonFamily,
                                  PhoneNumber = a.PhoneNumber,
                                  MedicalHistory = a.MedicalHistory,
                                  Diagnosis = a.Diagnosis,
                                  Treatment = a.Treatment,
                                  FitToFlyDate = a.FitToFlyDate?.ToString(GlobalConfig.DateFormat),
                                  ClassName = a.ClassID != null ? a.ClassMaster.ClassName : string.Empty,
                                  Airline = a.Airline,
                                  FlightHrs = TimeSpan.FromMinutes(int.Parse(a.FlightHrs)).ToString(),
                                  SpecialRequest = a.SpecialRequest,
                                  MedcrewAmbulanceAT = a.MedcrewAmbulanceAT,
                                  DoctorName = a.DoctorName,
                                  SignatureImageName = "/Files/SignatureImages/" + a.SignatureImageName,
                                  DoctorID = a.DoctorID,
                                  Paramedic = a.Paramedic,
                                  RecomdationName = a.RecomdationName,
                                  Driver = a.Driver,
                                  RepatriationNecessaryBecauseOf = (from b in _ctx.RepatriationNecessaryMasters
                                                                    select new RepatriationNecessaryTransactionModel
                                                                    {
                                                                        RepatriationNecessaryName = b.RepatriationName,
                                                                        RepatriationID = b.RepatriationID,
                                                                        isChecked = b.RepatriationNecessaryTransactions.Any(x => x.MedicalTransferID == MedicalTransferID)
                                                                    }).ToList(),

                                  RecomdationSubcategoryTransaction = (from c in _ctx.RecomdationSubcategories
                                                                       where c.RecomdationID == a.RecomdationId
                                                                       select new RecomdationSubTransactionModel
                                                                       {
                                                                           RecomdationSubName = c.RecomdationSubcategoryName,
                                                                           RecomdationID = c.RecomdationID,
                                                                           RecomdationSubcategoryID = c.RecomdationSubcategoryID,
                                                                           isChecked = c.RecomdationSubcategoryTransactions.Any(x => x.MedicalTransferID == MedicalTransferID)


                                                                       }).ToList(),
                                  RecomdationCategoryMaster = (from y in _ctx.RecomdationCategories
                                                               select new RecomdationCategoryModel
                                                               {
                                                                   RecomdationID = y.RecomdationID,
                                                                   RecomdationName = y.RecomdationName,
                                                               }).ToList(),
                                  AirClass = (from d in _ctx.ClassMasters
                                              select new AirClassModel
                                              {
                                                  ClassID = d.ClassID,
                                                  ClassName = d.ClassName
                                              }).ToList()


                              }).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            if (mResult.Result != null)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Record listed.";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "No record listed.";
            }
            return mResult;
        }

        public ResponseModel<ViewPdfModel> GetPdf(int MedicalTransferID)
        {
            ResponseModel<ViewPdfModel> mResult = new ResponseModel<ViewPdfModel>();
            mResult.Result = (from a in _ctx.MedicalTransferMasters
                              where a.MedicalTransferID == MedicalTransferID && a.isPdf == true
                              select new ViewPdfModel
                              {
                                  PdfPath = a.PdfPath != null ? GlobalConfig.baseUrl + "/Files/PDF/" + a.PdfPath : string.Empty,
                                  MedicalTransferID = a.MedicalTransferID
                              }).FirstOrDefault();
            mResult.Status = ResponseStatus.Success;
            return mResult;

        }

        public bool AddPdfIndatabase(int MedicalTransferID, string PdfPath)
        {
            MedicalTransferMaster result = _ctx.MedicalTransferMasters.FirstOrDefault(x => x.MedicalTransferID == MedicalTransferID);
            if (result != null)
            {
                result.isPdf = true;
                result.PdfPath = PdfPath;
                _ctx.SaveChanges();
                return true;
            }
            else
                return false;
        }

        public bool DeleteRecord(int MedicalTransferID)
        {
            MedicalTransferMaster result = _ctx.MedicalTransferMasters.FirstOrDefault(x => x.MedicalTransferID == MedicalTransferID);
            if (result != null)
            {
                _ctx.MedicalTransferMasters.Remove(result);
                _ctx.SaveChanges();
                return true;
            }
            return false;
        }

        public IQueryable<ViewMedicalTransferFormModel> GetAllMedicalTransferDetails()
        {
            return (from b in _ctx.MedicalTransferMasters
                    orderby b.MedicalTransferID descending
                    select new ViewMedicalTransferFormModel
                    {
                        AddedDate = b.RegisterDate,
                        PDFPath = b.PdfPath != null ? GlobalConfig.baseUrl + "/Files/PDF/" + b.PdfPath : string.Empty,
                        ContactPerson = b.ContactPerson,
                        PatientName = b.NameOfPatient,
                        isActive = b.isActive,
                        MedicalTransferID = b.MedicalTransferID,
                        isPdf = b.isPdf
                    });
        }

        public ResponseModel<object> DeleteTransferDetails(int Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            MedicalTransferMaster reuslt = _ctx.MedicalTransferMasters.FirstOrDefault(x => x.MedicalTransferID == Id);
            if (reuslt != null)
            {
                _ctx.MedicalTransferMasters.Remove(reuslt);
                _ctx.SaveChanges();
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Record details deleted.";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Record not found.";
            }
            return mResult;
        }

        public bool OpenReport(int MedicalTransferID)
        {
            MedicalTransferMaster result = _ctx.MedicalTransferMasters.FirstOrDefault(x => x.MedicalTransferID == MedicalTransferID);
            if (result != null)
            {
                result.isActive = true;
                _ctx.SaveChanges();
                return true;
            }
            else
                return false;
        }

        public bool ClosedReport(int MedicalTransferID)
        {
            MedicalTransferMaster result = _ctx.MedicalTransferMasters.FirstOrDefault(x => x.MedicalTransferID == MedicalTransferID);
            if (result != null)
            {
                result.isActive = false;
                _ctx.SaveChanges();
                return true;
            }
            else
                return false;
        }
    }
}