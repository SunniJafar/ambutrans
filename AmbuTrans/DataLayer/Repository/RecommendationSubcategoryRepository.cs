﻿using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmbuTrans.DataLayer.Repository
{
    public class RecommendationSubcategoryRepository : IDisposable
    {
        AmbuTransEntities _ctx;
        public RecommendationSubcategoryRepository()
        {
            _ctx = new AmbuTransEntities();
        }
        public void Dispose()
        {
            _ctx.Dispose();
        }
        //public IQueryable<RecomdationSubcategoryModel> GetSubcategory(int RecomdationID)
        //{
        //    return (from a in _ctx.RecomdationSubcategories
        //            where a.RecomdationID == RecomdationID
        //            orderby a.SubcategoryName ascending
        //            select new SubcategoryModel
        //            {
        //                SubcategoryID = a.SubcategoryID,
        //                SubcategoryName = a.SubcategoryName,
        //                CategoryID = a.CategoryID,
        //                ImageUrl = "/Images/SubcategoryImages/" + a.ImageName

        //            });
        //}
        public IQueryable<ViewRecomdationSubcategoryModel> GetSubcategoryList()
        {
            return (from a in _ctx.RecomdationSubcategories
                    join b in _ctx.RecomdationCategories on a.RecomdationID equals b.RecomdationID
                    orderby a.RecomdationSubcategoryName ascending
                    select new ViewRecomdationSubcategoryModel
                    {
                        RecomdationSubcategoryID = a.RecomdationSubcategoryID,
                        RecomdationSubcategoryName = a.RecomdationSubcategoryName,
                        RecomdationID = a.RecomdationID,
                        RecomdationName = b.RecomdationName,
                    });
        }
        public AddUpdateRecomdationSubcategoryModel FindById(int Id)
        {
            AddUpdateRecomdationSubcategoryModel e = (from x in _ctx.RecomdationSubcategories
                                                      where x.RecomdationSubcategoryID == Id
                                                      select new AddUpdateRecomdationSubcategoryModel
                                                      {
                                                          RecomdationID = x.RecomdationID,
                                                          RecomdationSubcategoryID = x.RecomdationSubcategoryID,
                                                          RecomdationSubcategoryName = x.RecomdationSubcategoryName,

                                                      }).FirstOrDefault();
            return e;

        }

        public ResponseModel<object> Delete(int Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            RecomdationSubcategory e = _ctx.RecomdationSubcategories.FirstOrDefault(x => x.RecomdationSubcategoryID == Id);
            if (e != null)
            {
                _ctx.RecomdationSubcategories.Remove(e);
                _ctx.SaveChanges();
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Record details deleted.";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Record not found.";
            }
            return mResult;
        }
        public int AddorUpdate(AddUpdateRecomdationSubcategoryModel model)
        {
            if (model.RecomdationSubcategoryID != 0)
            {
                RecomdationSubcategory s = _ctx.RecomdationSubcategories.SingleOrDefault(a => a.RecomdationSubcategoryID == model.RecomdationSubcategoryID);
                if (s != null)
                {
                    s.RecomdationSubcategoryName = model.RecomdationSubcategoryName;
                    s.RecomdationID = model.RecomdationID;
                    _ctx.SaveChanges();
                    return model.RecomdationID;
                }
            }
            else
            {
                RecomdationSubcategory subcategory = new RecomdationSubcategory();
                subcategory.RecomdationSubcategoryName = model.RecomdationSubcategoryName;
                subcategory.RecomdationID = model.RecomdationID;
                _ctx.RecomdationSubcategories.Add(subcategory);
                _ctx.SaveChanges();
            }
            return model.RecomdationID;

        }
    }
}