﻿using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmbuTrans.DataLayer.Repository
{
    public class MedicalReportRepository : IDisposable
    {
        AmbuTransEntities _ctx;
        public MedicalReportRepository()
        {
            _ctx = new AmbuTransEntities();
        }
        public void Dispose()
        {
            _ctx.Dispose();
        }
        public ResponseModel<List<ReportDetailsModel>> GetAllReports(int DoctorID)
        {
            ResponseModel<List<ReportDetailsModel>> mResult = new ResponseModel<List<ReportDetailsModel>>();
            mResult.Result = (from a in _ctx.MedicalTransferMasters
                              where a.DoctorID == DoctorID
                              orderby a.MedicalTransferID descending
                              select new
                              {
                                  MedicalTransferID = a.MedicalTransferID,
                                  NameOfPatient = a.NameOfPatient,
                                  ContactPerson = a.ContactPerson,
                                  AddedDate = a.RegisterDate,
                                  isActive = a.isActive,
                                  isPdf = a.isPdf,
                                  PdfName=a.PdfPath
                              }).ToList().Select(b => new ReportDetailsModel
                              {
                                  MedicalTransferID = b.MedicalTransferID,
                                  NameOfPatient = b.NameOfPatient,
                                  ContactPerson = b.ContactPerson,
                                  AddedDate = b.AddedDate.ToString(GlobalConfig.DateFormat),
                                  isActive = b.isActive,
                                  isPdf = b.isPdf,
                                  ReportFileUrl = GlobalConfig.baseUrl + "/Files/PDF/" + b.PdfName
                              }).ToList();
            return mResult;
        }
        public IQueryable<ViewMedicalReport> GetReports(int MedicalTransferID)
        {
            return (from a in _ctx.MedicalReportMasters
                    where a.MedicalTransferID == MedicalTransferID
                    select new ViewMedicalReport
                    {
                        MedicalTransferID = a.MedicalTransferID,
                        ReportFileID = a.ReportFileID,
                        ReportFileUrl = GlobalConfig.baseUrl + "/Files/MedicalReports/" + a.ReportFileName
                    });
        }

        public ResponseModel<object> AddMedicalReport_(AddMedicalReport model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            List<MedicalReportMaster> list = new List<MedicalReportMaster>();
            try
            {
                for (int i = 0; i < model.ReportFileName.Count; i++)
                {
                    list.Add(new MedicalReportMaster
                    {
                        ReportFileName = model.ReportFileName[i],
                        MedicalTransferID = model.MedicalTransferID,
                    });
                }
                _ctx.MedicalReportMasters.AddRange(list);
                _ctx.SaveChanges();

                mResult.Message = "Report added.";
                mResult.Status = ResponseStatus.Success;
            }
            catch (Exception error)
            {
                mResult.Message = error.Message;
                mResult.Status = ResponseStatus.Failed;

            }

            return mResult;
        }
        public bool DeleteReport(int ReportFileID)
        {
            MedicalReportMaster e = _ctx.MedicalReportMasters.FirstOrDefault(x => x.ReportFileID == ReportFileID);
            if (e != null)
            {
                string filepath = HttpContext.Current.Server.MapPath("/Files/MedicalReports/") + e.ReportFileName;
                if ((System.IO.File.Exists(filepath)))
                {
                    System.IO.File.Delete(filepath);
                }
                _ctx.MedicalReportMasters.Remove(e);
                _ctx.SaveChanges();
                return true;
            }
            return false;
        }
        public ResponseModel<List<ViewMedicalReport>> ViewMedicalReport(int MedicalTransferID)
        {
            ResponseModel<List<ViewMedicalReport>> mResult = new ResponseModel<List<ViewMedicalReport>>();
            mResult.Result = (from a in _ctx.MedicalReportMasters
                              where a.MedicalTransferID == MedicalTransferID
                              select new ViewMedicalReport
                              {

                                  ReportFileUrl = "/Files/MedicalReports/" + a.ReportFileName
                              }).ToList();
            return mResult;
        }
    }
}