﻿using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmbuTrans.DataLayer.Repository
{
    public class DoctorRepository : IDisposable
    {
        #region Connection
        AmbuTransEntities _ctx;
        public DoctorRepository()
        {
            _ctx = new AmbuTransEntities();
        }
        public void Dispose()
        {
            _ctx.Dispose();
        }
        #endregion
        public IQueryable<ViewDoctorModel> GetDoctorList()
        {
            return (from x in _ctx.DoctorMasters
                    orderby x.DoctorID descending
                    select new ViewDoctorModel
                    {
                        DoctorID = x.DoctorID,
                        DoctorName = x.DoctorName,
                        EmailID = x.EmailID,
                        isActive=x.IsActive,
                        RegisterDate=x.AddedDate
                        
                    });
        }
        public ResponseModel<object> Delete(int Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            DoctorMaster reuslt = _ctx.DoctorMasters.FirstOrDefault(x => x.DoctorID == Id);
            if (reuslt != null)
            {
                _ctx.DoctorMasters.Remove(reuslt);
                _ctx.SaveChanges();
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Record details deleted.";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Record not found.";
            }
            return mResult;
        }
        public bool AddorUpdate(AddUpdateDoctorModel model)
        {
            if (model.DoctorID != 0)
            {
                DoctorMaster Reg = _ctx.DoctorMasters.SingleOrDefault(a => a.DoctorID == model.DoctorID);
                if (Reg != null)
                {
                    Reg.DoctorName = model.DoctorName;
                    Reg.EmailID = model.EmailID;
                    Reg.Password = model.Password;
                    Reg.DeviceToken = model.DeviceToken;
                    Reg.Platform = model.Platform;
                    Reg.IsActive = false;
                    Reg.AddedDate = DateTime.Now;
                    _ctx.SaveChanges();
                    return true;
                }
            }
            else
            {
                DoctorMaster Reg = new DoctorMaster();
                Reg.DoctorName = model.DoctorName;
                Reg.EmailID = model.EmailID;
                Reg.Password = model.Password;
                Reg.DeviceToken = "DeviceToken";
                Reg.Platform = "Platform";
                Reg.IsActive = false;
                Reg.AddedDate = DateTime.Now;

                _ctx.DoctorMasters.Add(Reg);
                _ctx.SaveChanges();
                return true;

            }
            return false;
        }
        public AddUpdateDoctorModel FindById(int Id)
        {
            AddUpdateDoctorModel e = (from x in _ctx.DoctorMasters
                                        where x.DoctorID == Id
                                        select new AddUpdateDoctorModel
                                        {
                                            DoctorID = x.DoctorID,
                                            DoctorName = x.DoctorName,
                                            EmailID=x.EmailID
                                        }).FirstOrDefault();
            return e;

        }
        public bool Activate(int DooctorID)
        {
            DoctorMaster result = _ctx.DoctorMasters.FirstOrDefault(x => x.DoctorID == DooctorID);
            if (result != null)
            {
                result.IsActive = true;
                _ctx.SaveChanges();
                return true;
            }
            else
                return false;
        }
        public bool Deactivate(int DooctorID)
        {
            DoctorMaster result = _ctx.DoctorMasters.FirstOrDefault(x => x.DoctorID == DooctorID);
            if (result != null)
            {
                result.IsActive = false;
                _ctx.SaveChanges();
                return true;
            }
            else
                return false;
        }
    }
}   