﻿using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmbuTrans.DataLayer.Repository
{
    public class UtilityRepository : IDisposable
    {
        AmbuTransEntities _ctx;
        public UtilityRepository()
        {
            _ctx = new AmbuTransEntities();
        }
        public void Dispose()
        {
            _ctx.Dispose();
        }
        public ResponseModel<MasterApiModel> GetAllApiData()
        {
            ResponseModel<MasterApiModel> model = new ResponseModel<MasterApiModel>();

            model.Result.RecomdationCategorylist = (from x in _ctx.RecomdationCategories
                                                    orderby x.RecomdationID ascending
                                                    select new RecomdationCategoryModel
                                                    {
                                                        RecomdationID = x.RecomdationID,
                                                        RecomdationName = x.RecomdationName
                                                    }).ToList();

            model.Result.AirClasslist = (from x in _ctx.ClassMasters
                                         orderby x.ClassName ascending
                                         select new AirClassModel
                                         {
                                             ClassID = x.ClassID,
                                             ClassName = x.ClassName
                                         }).ToList();

            model.Result.RepatriationNecessaryList = (from x in _ctx.RepatriationNecessaryMasters
                                                      orderby x.RepatriationName ascending
                                                      select new RepatriationNecessaryModel
                                                      {
                                                          RepatriationID = x.RepatriationID,
                                                          RepatriationName = x.RepatriationName
                                                      }).ToList();
            return model;

        }
        public ResponseModel<AboutUsModel> GetAboutUs()
        {
            ResponseModel<AboutUsModel> model = new ResponseModel<AboutUsModel>();

            model.Result = (from x in _ctx.AboutUsMasters
                            select new AboutUsModel
                            {
                                Id = x.ID,
                                Description = x.AboutUs
                            }).FirstOrDefault();


            return model;

        }
        public ResponseModel<TermModel> TermsandConditions()
        {
            ResponseModel<TermModel> model = new ResponseModel<TermModel>();

            model.Result = (from x in _ctx.TermsandConditionsMasters
                            select new TermModel
                            {
                                Id = x.ID,
                                Description = x.TermsandConditions
                            }).FirstOrDefault();


            return model;

        }
        public IQueryable<RecomdationSubcategoryModel> GetSubcategory(int RecomdationID)
        {
            return (from a in _ctx.RecomdationSubcategories
                    where a.RecomdationID == RecomdationID
                    orderby a.RecomdationSubcategoryName ascending
                    select new RecomdationSubcategoryModel
                    {
                        RecomdationID = a.RecomdationID,
                        RecomdationSubcategoryID = a.RecomdationSubcategoryID,
                        RecomdationSubcategoryName = a.RecomdationSubcategoryName,
                    });
        }
        public IQueryable<ViewNotificationModel> GetMessage()
        {
            return (from a in _ctx.NotificationMasters
                    orderby a.NotificationID descending
                    select new ViewNotificationModel
                    {
                        NotificationID = a.NotificationID,
                        Message = a.Message,
                        AddedDate = a.DateTIme
                    });
        }
        public IQueryable<ViewViewForm_ReportStatusModel> GetAllFormStatus(int MedicalTransferID)
        {
            return (from a in _ctx.MedicalTransferMasters
                    where a.MedicalTransferID == MedicalTransferID
                    select new ViewViewForm_ReportStatusModel
                    {
                        MedicalTransferForm = true,
                        VitialInformationForm = a.VitalsInformationMasters.Any(x => x.MedicalTransferID == MedicalTransferID),
                        MedicalReport = a.MedicalReportMasters.Any(b => b.MedicalTransferID == MedicalTransferID)
                    });
        }
    }
}