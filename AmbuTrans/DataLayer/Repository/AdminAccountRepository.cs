﻿using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmbuTrans.DataLayer.Repository
{
    public class AdminAccountRepository:IDisposable
    {
        #region Connection
        AmbuTransEntities _ctx;
        public AdminAccountRepository()
        {
            _ctx = new AmbuTransEntities();
        }
        public void Dispose()
        {
            _ctx.Dispose();
        }
        #endregion
        public bool AdminLogin(AdminLoginModel model)
        {
            return _ctx.AdminMasters.Any(x => x.Username == model.username && x.Password == model.password);
        }
        public bool ChangePassword(AdminChangePasswordModel model)
        {
            AdminMaster admin = _ctx.AdminMasters.SingleOrDefault(a => a.Username==model.username && a.Password == model.currentPassword);
            if (admin != null)
            {
                admin.Password = model.ConfirmPassword;
                _ctx.SaveChanges();
                return true;
            }
            return false;
        }
    }
}