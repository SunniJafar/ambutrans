﻿using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmbuTrans.DataLayer.Repository
{
    public class RepatriationNecessaryRepository
    {
        #region Connection
        AmbuTransEntities _ctx;
        public RepatriationNecessaryRepository()
        {
            _ctx = new AmbuTransEntities();
        }
        public void Dispose()
        {
            _ctx.Dispose();
        }
        #endregion
        public IQueryable<RepatriationNecessaryModel> GetRepatriationList()
        {
            return (from x in _ctx.RepatriationNecessaryMasters
                    orderby x.RepatriationName ascending
                    select new RepatriationNecessaryModel
                    {
                        RepatriationID = x.RepatriationID,
                        RepatriationName = x.RepatriationName
                    });
        }
        public bool AddorUpdate(AddUpdateRepatriationNecessaryModel model)
        {
            if (model.RepatriationID != 0)
            {
                RepatriationNecessaryMaster c = _ctx.RepatriationNecessaryMasters.SingleOrDefault(a => a.RepatriationID == model.RepatriationID);
                if (c != null)
                {
                    c.RepatriationName = model.RepatriationName;
                    _ctx.SaveChanges();
                    return true;
                }
            }
            else
            {
                RepatriationNecessaryMaster result = new RepatriationNecessaryMaster();
                result.RepatriationName = model.RepatriationName;
                _ctx.RepatriationNecessaryMasters.Add(result);
                _ctx.SaveChanges();
                return true;

            }
            return false;
        }
        public AddUpdateRepatriationNecessaryModel FindById(int Id)
        {
            AddUpdateRepatriationNecessaryModel e = (from x in _ctx.RepatriationNecessaryMasters
                                        where x.RepatriationID == Id
                                        select new AddUpdateRepatriationNecessaryModel
                                        {
                                            RepatriationID = x.RepatriationID,
                                            RepatriationName = x.RepatriationName,
                                        }).FirstOrDefault();
            return e;

        }
        public ResponseModel<object> Delete(int Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            RepatriationNecessaryMaster e = _ctx.RepatriationNecessaryMasters.FirstOrDefault(x => x.RepatriationID == Id);
            if (e != null)
            {
                _ctx.RepatriationNecessaryMasters.Remove(e);
                _ctx.SaveChanges();
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Record details deleted.";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Record not found.";
            }
            return mResult;
        }
    }
}