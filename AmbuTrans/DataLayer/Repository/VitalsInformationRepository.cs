﻿using AmbuTrans.Helper;
using AmbuTrans.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmbuTrans.DataLayer.Repository
{
    public class VitalsInformationRepository : IDisposable
    {
        AmbuTransEntities _ctx;
        public VitalsInformationRepository()
        {
            _ctx = new AmbuTransEntities();
        }
        public void Dispose()
        {
            _ctx.Dispose();
        }
        public ResponseModel<object> AddUpdateVitalsInformation(AddVitalsInformationModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                if (model.VitalsInformationID != 0)
                {
                    VitalsInformationMaster result = _ctx.VitalsInformationMasters.FirstOrDefault(x => x.MedicalTransferID == model.MedicalTransferID);
                    if (result != null)
                    {
                        result.Length = model.Length;
                        result.Weight = model.Weight;
                        result.Station = Convert.ToInt32(model.Station);
                        result.Allergies = model.Allergies;
                        result.Mobility_disability = model.Mobility_disability;
                        result.Diet = model.Diet;
                        result.Secretion = model.Secretion;
                        result.Defecation = model.Defecation;
                        result.Urine = model.Urine;
                        result.PatientTransportableAgreed = model.PatientTransportableAgreed;
                        result.NRpolicy = model.NRpolicy;
                        result.Prognosis_Risks = model.Prognosis_Risks;
                        result.MRSA = model.MRSA;
                        result.Result = model.Result;
                        result.ProcedureExplained = model.ProcedureExplained;
                        result.GuidingPassenger = model.GuidingPassenger;
                        result.Bagage = model.Bagage;
                        result.Notes = model.Notes;
                        result.MedicationDescription = model.MedicationDescription;
                        //result.MedicalTransferID = model.MedicalTransferID;
                        _ctx.SaveChanges();


                        _ctx.VitalInformationMedicationTransactions.RemoveRange(_ctx.VitalInformationMedicationTransactions.Where(c => c.VitalsInformationID == model.VitalsInformationID));
                        _ctx.SaveChanges();


                        List<VitalInformationMedicationTransaction> transactionList = new List<VitalInformationMedicationTransaction>();

                        foreach (var item in model.MedicationTransaction)
                        {
                            transactionList.Add(new VitalInformationMedicationTransaction()
                            {
                                ActualVitals_Time = item.ActualVitals_Time,
                                Temprature = item.Temprature,
                                RespiratoryRate = item.RespiratoryRate,
                                Saturation = item.Saturation,
                                HeartRate = item.HeartRate,
                                BloodPressure1 = item.BloodPressure1,
                                BloodPressure2 = item.BloodPressure2,
                                EMV = item.EMV,
                                VitalsInformationID = model.VitalsInformationID
                            });
                        }
                        _ctx.VitalInformationMedicationTransactions.AddRange(transactionList);
                        _ctx.SaveChanges();

                        mResult.Result = model;
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = "Details Updated successfully";
                    }
                    else
                    {
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = "Details not Update";
                    }
                }
                else
                {
                    VitalsInformationMaster vitals = new VitalsInformationMaster();
                    vitals.Length = model.Length;
                    vitals.Weight = model.Weight;
                    vitals.Station = Convert.ToInt32(model.Station);
                    vitals.Allergies = model.Allergies;
                    vitals.Mobility_disability = model.Mobility_disability;
                    vitals.Diet = model.Diet;
                    vitals.Secretion = model.Secretion;
                    vitals.Defecation = model.Defecation;
                    vitals.Urine = model.Urine;
                    vitals.PatientTransportableAgreed = model.PatientTransportableAgreed;
                    vitals.NRpolicy = model.NRpolicy;
                    vitals.Prognosis_Risks = model.Prognosis_Risks;
                    vitals.MRSA = model.MRSA;
                    vitals.Result = model.Result;
                    vitals.ProcedureExplained = model.ProcedureExplained;
                    vitals.GuidingPassenger = model.GuidingPassenger;
                    vitals.Bagage = model.Bagage;
                    vitals.Notes = model.Notes;
                    vitals.MedicationDescription = model.MedicationDescription;
                    vitals.MedicalTransferID = model.MedicalTransferID;
                    _ctx.VitalsInformationMasters.Add(vitals);
                    _ctx.SaveChanges();
                    model.VitalsInformationID = vitals.VitalsInformationID;

                    List<VitalInformationMedicationTransaction> transactionList = new List<VitalInformationMedicationTransaction>();
                    if (model.MedicationTransaction != null)
                    {

                        foreach (var item in model.MedicationTransaction)
                        {
                            transactionList.Add(new VitalInformationMedicationTransaction()
                            {
                                ActualVitals_Time = item.ActualVitals_Time,
                                Temprature = item.Temprature,
                                RespiratoryRate = item.RespiratoryRate,
                                Saturation = item.Saturation,
                                HeartRate = item.HeartRate,
                                BloodPressure1 = item.BloodPressure1,
                                BloodPressure2 = item.BloodPressure2,
                                EMV = item.EMV,
                                VitalsInformationID = model.VitalsInformationID
                            });
                        }
                        _ctx.VitalInformationMedicationTransactions.AddRange(transactionList);
                        _ctx.SaveChanges();
                    }

                    mResult.Result = model;
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Record submited.";
                }
            }
            catch (Exception)
            {
                mResult.Result = null;
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Record not submit.";
                throw;
            }
            return mResult;
        }

        public ResponseModel<object> UpdateDetails(EditVitalsInformationModel model)
        {

            ResponseModel<object> mResult = new ResponseModel<object>();
            VitalsInformationMaster result = _ctx.VitalsInformationMasters.FirstOrDefault(x => x.MedicalTransferID == model.MedicalTransferID);
            if (result != null)
            {
                result.Length = model.Length;
                result.Weight = model.Weight;
                result.Station = Convert.ToInt32(model.Station);
                result.Allergies = model.Allergies;
                result.Mobility_disability = model.Mobility_disability;
                result.Diet = model.Diet;
                result.Secretion = model.Secretion;
                result.Defecation = model.Defecation;
                result.Urine = model.Urine;
                result.PatientTransportableAgreed = model.PatientTransportableAgreed;
                result.NRpolicy = model.NRpolicy;
                result.Prognosis_Risks = model.Prognosis_Risks;
                result.MRSA = model.MRSA;
                result.Result = model.Result;
                result.ProcedureExplained = model.ProcedureExplained;
                result.GuidingPassenger = model.GuidingPassenger;
                result.Bagage = model.Bagage;
                result.Notes = model.Notes;
                result.MedicationDescription = model.MedicationDescription;
                //result.MedicalTransferID = model.MedicalTransferID;
                _ctx.SaveChanges();


                _ctx.VitalInformationMedicationTransactions.RemoveRange(_ctx.VitalInformationMedicationTransactions.Where(c => c.VitalsInformationID == model.VitalsInformationID));
                _ctx.SaveChanges();


                List<VitalInformationMedicationTransaction> transactionList = new List<VitalInformationMedicationTransaction>();

                foreach (var item in model.MedicationTransaction)
                {
                    transactionList.Add(new VitalInformationMedicationTransaction()
                    {
                        ActualVitals_Time = item.ActualVitals_Time,
                        Temprature = item.Temprature,
                        RespiratoryRate = item.RespiratoryRate,
                        Saturation = item.Saturation,
                        HeartRate = item.HeartRate,
                        BloodPressure1 = item.BloodPressure1,
                        BloodPressure2 = item.BloodPressure2,
                        EMV = item.EMV,
                        VitalsInformationID = model.VitalsInformationID
                    });
                }
                _ctx.VitalInformationMedicationTransactions.AddRange(transactionList);
                _ctx.SaveChanges();

                mResult.Result = model;
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Details Updated successfully";



            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Details not Update";
            }
            return mResult;
        }

        public ResponseModel<EditVitalsInformationModel> FindById(int MedicalTransferID)
        {
            ResponseModel<EditVitalsInformationModel> mResult = new ResponseModel<EditVitalsInformationModel>();
            EditVitalsInformationModel result = (from x in _ctx.VitalsInformationMasters
                                                 where x.MedicalTransferID == MedicalTransferID
                                                 select new EditVitalsInformationModel
                                                 {
                                                     VitalsInformationID = x.VitalsInformationID,
                                                     Length = x.Length,
                                                     Weight = x.Weight,
                                                     Station = x.Station == 0 ? Station.ICU : Station.Normal_Station,
                                                     Allergies = x.Allergies,
                                                     Mobility_disability = x.Mobility_disability,
                                                     Diet = x.Diet,
                                                     Secretion = x.Secretion,
                                                     Defecation = x.Defecation,
                                                     Urine = x.Urine,
                                                     PatientTransportableAgreed = x.PatientTransportableAgreed,
                                                     NRpolicy = x.NRpolicy,
                                                     Prognosis_Risks = x.Prognosis_Risks,
                                                     MRSA = x.MRSA,
                                                     Result = x.Result,
                                                     ProcedureExplained = x.ProcedureExplained,
                                                     GuidingPassenger = x.GuidingPassenger,
                                                     Bagage = x.Bagage,
                                                     Notes = x.Notes,
                                                     MedicationDescription = x.MedicationDescription,
                                                     MedicalTransferID = x.MedicalTransferID,
                                                     MedicationTransaction = (from a in _ctx.VitalInformationMedicationTransactions
                                                                              where a.VitalsInformationID == x.VitalsInformationID
                                                                              select new VitalInformationMedicationTransactionModel
                                                                              {
                                                                                  ActualVitals_Time = a.ActualVitals_Time,
                                                                                  Temprature = a.Temprature,
                                                                                  RespiratoryRate = a.RespiratoryRate,
                                                                                  Saturation = a.Saturation,
                                                                                  HeartRate = a.HeartRate,
                                                                                  BloodPressure1 = a.BloodPressure1,
                                                                                  BloodPressure2 = a.BloodPressure2,
                                                                                  EMV = a.EMV,
                                                                                  VitalsInformationID = a.VitalsInformationID

                                                                              }).ToList()
                                                 }).FirstOrDefault();




            if (result != null)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Record listed.";
                mResult.Result = result;
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "No record listed.";
            }
            return mResult;
        }


    }
}