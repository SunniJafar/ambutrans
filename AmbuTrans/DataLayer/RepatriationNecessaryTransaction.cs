//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AmbuTrans.DataLayer
{
    using System;
    using System.Collections.Generic;
    
    public partial class RepatriationNecessaryTransaction
    {
        public int RepatriationTransactionID { get; set; }
        public int RepatriationID { get; set; }
        public int MedicalTransferID { get; set; }
    
        public virtual MedicalTransferMaster MedicalTransferMaster { get; set; }
        public virtual RepatriationNecessaryMaster RepatriationNecessaryMaster { get; set; }
    }
}
