﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace AmbuTrans.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //************************ CSS ************************

            bundles.Add(new StyleBundle("~/css/bundles").Include(
                      "~/Assets/Plugins/Bootstrap/CSS/bootstrap.css",
                      "~/Assets/Plugins/Bootstrap/CSS/bootstrap-extended.css",
                      "~/Assets/CSS/Core/app.css",
                      "~/Assets/CSS/Core/colors.css",
                      "~/Assets/CSS/Fonts/icomoon.css",
                      "~/Assets/Plugins/pace/pace.css",
                      "~/Assets/CSS/Custom/custom.css",
                      "~/Assets/Plugins/animate/animate.css",
                       "~/Assets/Plugins/sweetalert/sweetalert.css"
                      ));

            bundles.Add(new StyleBundle("~/css/menu").Include(
                    "~/Assets/CSS/Menu/vertical-menu.css",
                    "~/Assets/CSS/Menu/vertical-overlay-menu.css"
                    ));

            bundles.Add(new StyleBundle("~/css/icheck").Include(
                    "~/Assets/Plugins/iCheck/icheck-blue.css"
                    ));

            bundles.Add(new StyleBundle("~/css/login-register").Include(
                    "~/Assets/Pages/login-register/login-register.css"
                    ));

            bundles.Add(new StyleBundle("~/css/gridMvc").Include(
                    "~/Assets/Plugins/Gridmvc/CSS/Gridmvc.css",
                    "~/Assets/Plugins/Gridmvc/CSS/PagedList.css"
                    ));

            bundles.Add(new StyleBundle("~/css/uploadImage").Include(
                    "~/Assets/Plugins/UploadImage/cropper.css",
                    "~/Assets/Plugins/UploadImage/uploadImage.css"
                    ));

            bundles.Add(new StyleBundle("~/css/wizard").Include(
                    "~/Assets/Plugins/wizard/wizard.css"
                    ));

            bundles.Add(new StyleBundle("~/css/jsgrid").Include(
                   "~/Assets/Plugins/jsGrid/jsgrid-theme.min.css",
                   "~/Assets/Plugins/jsGrid/jsgrid.min.css"
                   ));

            bundles.Add(new StyleBundle("~/css/fileinput").Include(
                  "~/Assets/Plugins/fileinput/css/fileinput.min.css",
                  "~/Assets/Plugins/fileinput/themes/explorer/theme.min.css"
                  ));

            //************************ jquery ************************

            bundles.Add(new ScriptBundle("~/jQuery/bundles").Include(
                    "~/Assets/Jquery/main/jquery.min.js",
                    "~/Assets/Jquery/ui/tether.min.js",
                    "~/Assets/Plugins/Bootstrap/Jquery/bootstrap.min.js",
                    "~/Assets/Plugins/pace/pace.min.js",
                    "~/Assets/Jquery/scripts.js"
                    ));

            bundles.Add(new ScriptBundle("~/jQuery/core").Include(
                   "~/Assets/Jquery/Core/app-menu.js",
                   "~/Assets/Jquery/Core/app.js"
                   ));

            bundles.Add(new ScriptBundle("~/jQuery/ui").Include(
                  "~/Assets/Jquery/ui/blockUI.min.js",
                  "~/Assets/Jquery/ui/jquery-sliding-menu.js",
                  "~/Assets/Jquery/ui/jquery.matchHeight-min.js",
                  "~/Assets/Jquery/ui/perfect-scrollbar.jquery.min.js",
                  "~/Assets/Jquery/ui/screenfull.min.js",
                  "~/Assets/Jquery/ui/unison.min.js",
                  "~/Assets/Plugins/blockUI/blockUI.min.js",
                  "~/Assets/Plugins/sweetalert/sweetalert.min.js"
                  ));

            bundles.Add(new ScriptBundle("~/jQuery/validate").Include(
                    "~/Assets/Plugins/Validate/jquery.validate.min.js",
                    "~/Assets/Plugins/Validate/jquery.validate.unobtrusive.min.js",
                    "~/Assets/Plugins/Validate/jquery.unobtrusive-ajax.min.js"
                    ));

            bundles.Add(new ScriptBundle("~/jQuery/iCheck").Include(
                   "~/Assets/Plugins/iCheck/icheck.min.js"
                   ));

            bundles.Add(new ScriptBundle("~/jQuery/login-register").Include(
                   "~/Assets/Pages/login-register/form-login-register.js"
                   ));

            bundles.Add(new ScriptBundle("~/jQuery/gridMvc").Include(
                  "~/Assets/Plugins/Gridmvc/Jquery/gridmvc.js"
                  ));

            bundles.Add(new ScriptBundle("~/jQuery/uploadImage").Include(
                 "~/Assets/Plugins/UploadImage/cropper.js",
                 "~/Assets/Plugins/UploadImage/uploadImage.js"
                 ));

            bundles.Add(new ScriptBundle("~/jQuery/stepWizard").Include(
                 "~/Assets/Plugins/wizard/jquery.steps.min.js",
                 "~/Assets/Plugins/wizard/wizard-steps.js"
                 ));

            bundles.Add(new ScriptBundle("~/jQuery/jsgrid").Include(
                 "~/Assets/Plugins/jsgrid/jsgrid.min.js"
                 ));

            bundles.Add(new ScriptBundle("~/jQuery/fileinput").Include(
                  "~/Assets/Plugins/fileinput/js/fileinput.min.js",
                  "~/Assets/Plugins/fileinput/js/plugins/sortable.min.js",
                  "~/Assets/Plugins/fileinput/js/fileinput-script.js"
                 ));

            BundleTable.EnableOptimizations = true;
        }
    }
}