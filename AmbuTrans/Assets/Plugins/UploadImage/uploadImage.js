﻿
function SaveCropedImage(saveFilePath, hdnImageName, postUrl) {
    StartBlockPage();
    var $image = $("#cropimage_" + hdnImageName);
    var result = $image.cropper("getCroppedCanvas");
    var imgData = result.toDataURL('image/png');

    var extension = $("#fileImage_" + hdnImageName).val().split('.').pop();
    var prevImageName = $("#prev_" + hdnImageName).val();

    var model = {
        imgBase64String: imgData,
        saveFilePath: saveFilePath,
        extension: extension,
        previousImageName: prevImageName
    };

    $.ajax({
        type: 'POST',
        url: postUrl,
        data: JSON.stringify(model),
        contentType: 'application/json; charset=utf-8',
        dataType: 'JSON',
        success: function (resultResponse) {
            $("#solid-alerts").show();
            $('#solid-alerts #message').text(resultResponse.Message);
            if (resultResponse.Status == 1) {
                $("#solid-alerts .alert").addClass("bg-primary");
            }
            else {
                $("#solid-alerts .alert").addClass("bg-warning");
            }

            $('#preview_' + hdnImageName).attr('src', imgData);
            $("#" + hdnImageName).val(resultResponse.Result);
            $("#prev_" + hdnImageName).val(resultResponse.Result);
            $("#Modal_" + hdnImageName).modal('hide');
            CloseBlockPage();
        },
        error: function (e) {
            $("#Modal_" + hdnImageName).modal('hide');
            $("#solid-alerts .alert").addClass("bg-danger");
            $("#solid-alerts").show();
            $('#solid-alerts #message').text(e.status + "," + e.statusText);
            CloseBlockPage();
        }
    });
}