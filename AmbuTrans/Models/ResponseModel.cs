﻿using AmbuTrans.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmbuTrans.Models
{
    public class ResponseModel<T> where T : new()
    {
        public ResponseModel()
        {
            Status = ResponseStatus.Failed;
            Result = new T();
            Message = string.Empty;
        }
        public ResponseStatus Status { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }
    }
}