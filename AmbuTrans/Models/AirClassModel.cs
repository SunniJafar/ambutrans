﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AmbuTrans.Models
{
    public class AddUpdateAirClassModel
    {
        public int ClassID { get; set; }
        [Required]
        [DisplayName("Class Name")]
        public string ClassName { get; set; }
    }
}