﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AmbuTrans.Models
{
    public class ViewDoctorModel
    {
        public int DoctorID { get; set; }
        public string EmailID { get; set; }
        public string DoctorName { get; set; }
        public bool isActive { get; set; }
        public DateTime RegisterDate { get; set; }

    }
    public class AddUpdateDoctorModel
    {
        public int DoctorID { get; set; }
        [Required]
        public string DoctorName { get; set; }
        [Required]
        public string EmailID { get; set; }
        [Required]
        public string Password { get; set; }
        public string DeviceToken { get; set; }
        public string Platform { get; set; }

        
    }
}