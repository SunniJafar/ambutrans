﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AmbuTrans.Models
{
    public class AddUpdateRecomdationSubcategoryModel
    {
        [Required]
        [DisplayName("Recomdation Name")]
        public int RecomdationID { get; set; }
        [Required]
        [DisplayName("Recomdation Subcategory Name")]
        public string RecomdationSubcategoryName { get; set; }
        public int RecomdationSubcategoryID { get; set; }
    }
    public class ViewRecomdationSubcategoryModel
    {
        public int RecomdationID { get; set; }
        public string RecomdationSubcategoryName { get; set; }
        public int RecomdationSubcategoryID { get; set; }
        public string RecomdationName { get; set; }

    }
}