﻿using AmbuTrans.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AmbuTrans.Models
{
    public class AlertModel
    {
        public AlertModel(AlertStatus alertStatus, string alertMessage)
        {
            this.alertStatus = alertStatus;
            this.alertMessage = alertMessage;
        }
        public AlertStatus alertStatus { get; set; }
        public string alertMessage { get; set; }
    }

    public class BreadcrumbModel
    {
        public string actionTitle { get; set; }
        public string actionName { get; set; }
        public string actioncontroller { get; set; }
        public string subActionTitle { get; set; }

    }
    public class AirClassModel
    {
        public int ClassID { get; set; }
        public string ClassName { get; set; }
    }
    public class RecomdationCategoryModel
    {
        public int RecomdationID { get; set; }
        public string RecomdationName { get; set; }
    }
    public class RecomdationSubcategoryModel
    {
        public int RecomdationSubcategoryID { get; set; }
        public string RecomdationSubcategoryName { get; set; }
        public int RecomdationID { get; set; }
    }
    public class RecomdationSubcategoryTransactionModel
    {
        public int MedicalTransferID { get; set; }
        public int RecomdationSubcategoryID { get; set; }
    }
    public class RepatriationNecessaryModel
    {
        public int RepatriationID { get; set; }
        public string RepatriationName { get; set; }
    }
    public class RepatriationNecessaryTransactionModel
    {
        public int MedicalTransferID { get; set; }
        public int RepatriationID { get; set; }
        public string RepatriationNecessaryName { get; set; }
        public bool isChecked { get; set; }


    }
    public class AddUpdateRepatriationNecessaryModel
    {
        public int RepatriationID { get; set; }
        [Required]
        public string RepatriationName { get; set; }
    }
}