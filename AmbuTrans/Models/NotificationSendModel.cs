﻿using AmbuTrans.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AmbuTrans.Models
{
    public class NotificationSendModel
    {
        public NotificationSendModel()
        {
            NotificationPageType = NotificationPageTypes.Notification;
        }
        public string NotificationText { get; set; }

        public List<DeviceTokenModel> DeviceTokenList { get; set; }

        public NotificationStatus NotificationStatus { get; set; }

        public NotificationPageTypes NotificationPageType { get; set; }

        public string TablePrimaryID { get; set; }

        public string SenderID { get; set; }

        public string SenderName { get; set; }

        public Role SenderRole { get; set; }

        public int TotalUnreadNotificationCount { get; set; }
    }
    public class DeviceTokenModel
    {
        public string DeviceToken { get; set; }
        public string Platform { get; set; }
    }
    public class ViewNotificationModel
    {
        public int NotificationID { get; set; }
        public string Message { get; set; }
        public System.DateTime AddedDate { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
    }
    public class ViewViewForm_ReportStatusModel
    {
        public bool MedicalTransferForm { get; set; }
        public bool VitialInformationForm { get; set; }
        public bool MedicalReport { get; set; }
    }
    public class SendMessageModel
    {
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }
        public string Time { get; set; }
    }
    public class ViewMessageModel
    {
        public int MessageID { get; set; }
        public string Message { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public DateTime DateTIme { get; set; }

    }
}