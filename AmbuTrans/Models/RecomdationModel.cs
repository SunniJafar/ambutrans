﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AmbuTrans.Models
{
    public class AddUpdateRecomdationModel
    {
        [Required(ErrorMessage = "Please enter recomdation Name")]
        public string RecomdationName { get; set; }
        public int RecomdationID { get; set; }
    }
}