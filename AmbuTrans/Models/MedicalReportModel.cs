﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmbuTrans.Models
{
    public class AddMedicalReport
    {
        public int ReportFileID { get; set; }
        public List<string> ReportFileName { get; set; }
        public int MedicalTransferID { get; set; }
    }
    public class DeleteMedicalReport
    {
        public int ReportFileID { get; set; }
    }
    public class ReportDetailsModel
    {
        public int MedicalTransferID { get; set; }
        public string NameOfPatient { get; set; }
        public string ContactPerson { get; set; }
        public string AddedDate { get; set; }
        public bool? isPdf { get; set; }
        public int ReportStatus { get; set; }
        public bool? isActive { get; set; }
        public string ReportFileUrl { get; set; }
    }
    public class MedicalReportDetails
    {
        public List<string> ReportFileName { get; set; }
        public int MedicalTransferID { get; set; }
    }
    public class ViewMedicalReport
    {
        public int ReportFileID { get; set; }
        public int? MedicalTransferID { get; set; }
        public string ReportFileUrl { get; set; }
    }
    public class MasterApiModel
    {
        public List<AirClassModel> AirClasslist { get; set; }
        public List<RecomdationCategoryModel> RecomdationCategorylist { get; set; }
        public List<RepatriationNecessaryModel> RepatriationNecessaryList { get; set; }
    }
    public class AboutUsModel
    {
        public string Description { get; set; }
        public int Id { get; set; }
    }
    public class TermModel
    {
        public string Description { get; set; }
        public int Id { get; set; }
    }
    public class ViewPdfModel
    {
        public string PdfPath { get; set; }
        public int MedicalTransferID { get; set; }
    }
    public class ReportViewDetails
    {
        public List<ViewMedicalReport> ViewReport { get; set; }
    }
}