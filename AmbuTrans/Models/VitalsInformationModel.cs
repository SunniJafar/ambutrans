﻿using AmbuTrans.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmbuTrans.Models
{
    public class AddVitalsInformationModel
    {
        public int VitalsInformationID { get; set; }
        public string Length { get; set; }
        public string Weight { get; set; }
        public Station Station { get; set; }
        public string Allergies { get; set; }
        public string Mobility_disability { get; set; }
        public string Diet { get; set; }
        public string Secretion { get; set; }
        public string Defecation { get; set; }
        public string Urine { get; set; }
        public bool PatientTransportableAgreed { get; set; }
        public string NRpolicy { get; set; }
        public string Prognosis_Risks { get; set; }
        public bool MRSA { get; set; }
        public bool Result { get; set; }
        public bool ProcedureExplained { get; set; }
        public string GuidingPassenger { get; set; }
        public string Bagage { get; set; }
        public string Notes { get; set; }
        public int MedicalTransferID { get; set; }
        public string MedicationDescription { get; set; }
        public List<VitalInformationMedicationTransactionModel> MedicationTransaction { get; set; }
    }
    public class EditVitalsInformationModel : AddVitalsInformationModel
    {

    }
    public class VitalInformationMedicationTransactionModel
    {
        public string ActualVitals_Time { get; set; }
        public int? Temprature { get; set; }
        public int? RespiratoryRate { get; set; }
        public int? Saturation { get; set; }
        public int? HeartRate { get; set; }
        public int? BloodPressure1 { get; set; }
        public int? BloodPressure2 { get; set; }
        public int? EMV { get; set; }
        public int? VitalsInformationID { get; set; }
    }
    public class ViewVitalsInformationModel : AddVitalsInformationModel
    {
    }
}