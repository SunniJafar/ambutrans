﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmbuTrans.Models
{
    public class AddMedicalTransferModel
    {
        public int MedicalTransferID { get; set; }
        public System.DateTime RegisterDate { get; set; }
        public string NameOfPatient { get; set; }
        public string ToWhomItMayConcern { get; set; }
        public int? Age { get; set; }
        public bool Gender { get; set; }
        public System.DateTime? DateOfBirth { get; set; }
        public string PresentedInHospital { get; set; }
        public string HospitalAddress { get; set; }
        public System.DateTime? Date { get; set; }
        public string HospitalizedTo { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPersonAddress { get; set; }
        public bool BedArranged { get; set; }
        public System.DateTime? TransferArrivalDate { get; set; }
        public string Time { get; set; }
        public string ContactPersonFamily { get; set; }
        public string PhoneNumber { get; set; }
        public string MedicalHistory { get; set; }
        public string Diagnosis { get; set; }
        public string Treatment { get; set; }
        public System.DateTime? FitToFlyDate { get; set; }
        public int? ClassID { get; set; }
        public string Airline { get; set; }
        public string FlightHrs { get; set; }
        public string SpecialRequest { get; set; }
        public string MedcrewAmbulanceAT { get; set; }
        public string DoctorName { get; set; }
        public string SignatureImageName { get; set; }
        public int DoctorID { get; set; }
        public string Paramedic { get; set; }
        public string Driver { get; set; }
        public List<RepatriationNecessaryTransactionModel> RepatriationNecessaryBecauseOf { get; set; }
        public List<RecomdationSubTransactionModel> RecomdationSubcategoryTransaction { get; set; }
    }
    public class EditMedicalTransferModel : AddMedicalTransferModel
    {

    }
    public class RecomdationSubTransactionModel
    {
        public int MedicalTransferID { get; set; }
        public int RecomdationSubcategoryID { get; set; }
        public string RecomdationSubName { get; set; }
        public int RecomdationID { get; set; }
        public bool isChecked { get; set; }


    }
    public class ViewMedicalTransferModel
    {
        public int MedicalTransferID { get; set; }
        public string RegisterDate { get; set; }
        public string NameOfPatient { get; set; }
        public string ToWhomItMayConcern { get; set; }
        public int? Age { get; set; }
        public bool Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string PresentedInHospital { get; set; }
        public string HospitalAddress { get; set; }
        public string Date { get; set; }
        public string HospitalizedTo { get; set; }
        public string ContactPerson { get; set; }
        public string ContactPersonAddress { get; set; }
        public bool BedArranged { get; set; }
        public string TransferArrivalDate { get; set; }
        public string Time { get; set; }
        public string ContactPersonFamily { get; set; }
        public string PhoneNumber { get; set; }
        public string MedicalHistory { get; set; }
        public string Diagnosis { get; set; }
        public string Treatment { get; set; }
        public string FitToFlyDate { get; set; }
        public string ClassName { get; set; }
        public int? ClassID { get; set; }
        public string Airline { get; set; }
        public string FlightHrs { get; set; }
        public string SpecialRequest { get; set; }
        public string MedcrewAmbulanceAT { get; set; }
        public string DoctorName { get; set; }
        public string SignatureImageName { get; set; }
        public int DoctorID { get; set; }
        public string Paramedic { get; set; }
        public string Driver { get; set; }
        public string RecomdationName { get; set; }
        public List<RepatriationNecessaryTransactionModel> RepatriationNecessaryBecauseOf { get; set; }
        public List<RecomdationSubTransactionModel> RecomdationSubcategoryTransaction { get; set; }
        public List<RecomdationCategoryModel> RecomdationCategoryMaster { get; set; }
        public List<AirClassModel> AirClass { get; set; }
        public int DummyId { get; set; }



        public string ConvertDate { get; set; }
    }
    public class ViewMedicalTransferFormModel
    {
        public int MedicalTransferID { get; set; }
        public string PatientName { get; set; }
        public string ContactPerson { get; set; }
        public DateTime AddedDate { get; set; }
        public bool isActive { get; set; }
        public bool isPdf { get; set; }
        public string PDFPath { get; set; }
    }
}