﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmbuTrans.Models
{
    public class LoginModel
    {
        public int DoctorID { get; set; }
        public string EmailID { get; set; }
        public string Password { get; set; }
        public string DeviceToken { get; set; }
        public string Platform { get; set; }
    }

    public class RegisterModel : LoginModel
    {
        public string DoctorName { get; set; }
    }

    public class ForgetPasswordModel
    {
        public string EmailID { get; set; }
    }

    public class ChangePasswordModel
    {
        public int DoctorID { get; set; }
        public string EmailID { get; set; }
        public string NewPassword { get; set; }
        public string OldPassword { get; set; }
    }
}